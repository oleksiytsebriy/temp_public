from django.shortcuts import render

# from django.http import HttpResponse
# Create your views here.
def main(request):
    # return HttpResponse('<p>hello</p>')

    # return render(request, 'main.html')

    origin = request.GET.get('origin', None)
    destination = request.GET.get('destination', None)
    steps = ['step0', 'step1', 'step2']
    if  origin is None:
        origin= 'Demo origin'
    if destination is None:
        destination = 'Demo destination'
    content = {
        'origin': origin,
        'destination': destination,
        'steps': steps
    }
    return render(request, 'main.html', content)