'''
v_10

implemented Word embeddings
    uses the word2vec pre-trained glove.6B.50d.txt

approach:
* input now is not integer or one-hot but 50- or 300- dim vector
* output is still one-hot
* then use top 5 of max values and calculate the resulting vector  sum(p_i * vec_i)
    actually - looks as only first is relevant
* build clusters being based on training set
Not implemented yet:
    * cluster all predictions from training set
    * compute prediction for unseen sentence and use knn to assign the placeholder to some category

===

v_9

Multi bidirectional layers implemented


training sticks
training sticks on accuracy 67 %  even for one layer ... why ???
===========================================


    tf.contrib.rnn.stack_bidirectional_dynamic_rnn

# Other Todo
    word embeddings !!!
    ! nmt model
    Train on larger corpus


v_8

the bidirectional manually as two lstm
uses two separated inputs contexts: x_fw, x_bw
uses two separated parameters:  and just add the results at the last (output layer)

v_7

Implemented :
 bidirectional
 1) input is input+ target+ input e.g. len of 9 (4+1+4 ) vector
 2) it uses the only layer of
 3) use the rnn bidirectional

------------- Accuracy is 86 % ---------------



v_6: Implemented :
    minibatch  [General Accuracy: 88.0%]
v_5
    sampling (no just argmax only prediction)
    got 2 layers (lstm)
    restoring from graph

v_4
    saving restoring models
    this does not use graph and collections explicitly but creation from scratch at restoring
v_3
    *  better structure
    *  save and load  the model

v_2
    Code is based on sample:
    https://towardsdatascience.com/lstm-by-example-using-tensorflow-feb0c1968537

    it considers 3 words (n_input)
    the input data is 3 integers NOT the one-hot vector
    but output data is probabilities distribution vector

    The dimensions are modified comparing to ones in the sample due to no need of redundant dimension

v_1
    This model does not train  - it starts to generate only ' ' and cost does not get reduced after few iterations
    the cost is calculated at every time step - not as in the sample  provided with keras https://github.com/keras-team/keras/blob/master/examples/lstm_text_generation.py

    This might be the language model (character level) being based on numpy implementation (sampel of coursera) : 'rnn_character_level_language_model.py'
    Also used the first_neural_network_in_tensorflow.py as template

    manually implemented RNN level with the help of tf methods

    each example induces x and y by excluding first and last symbol correspondingly e.g. hello my darling => x = [hello my darlin], y =[ello my darling]
    cost is calculated at every time step but not only on final time step



'''


from string import ascii_lowercase
import re
import collections
import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn
import os

import random
import math
import time

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def load_data(training_corpus_file_name, pre_trained_vec_file_name):

    ''':returns ndarray of words ,  '''

    # read training corpus
    print('reading training corpus')
    with open(training_corpus_file_name) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    content = [word for i in range(len(content)) for word in content[i].split()] # currently there is only line but in case of multi line...
    content.append('_target_')
    content = np.array(content)
    print('Words amount: {} '.format(len(content)-1))

    # read pretrained vectors
    print('reading pretrained vectors')
    start_time = time.time()

    words, word_to_vec_map = read_glove_vecs(pre_trained_vec_file_name)
    time_loading = time.time()
    print('loading: {:.3f}s'.format(time_loading - start_time))
    print('len (words) = {:,}'.format(len(words)))
    vec_dim = word_to_vec_map['hello'].shape[0]
    print('dimension of vector = {:,}'.format(vec_dim ))



    word_ind, ind_word = build_dataset(content)
    vocab_size = len(word_ind)  # n_x
    print('Vocab_size : {:,}'.format(vocab_size))

    X = []
    X_demo_context = []
    Y = []
    cursor = 0
    while cursor + 2 * n_input < len(content)-1: # -1 = due to added the target word to vocabulary
        x_context_left = [word_to_vec(str(content[i]), word_to_vec_map) for i in range(cursor, cursor + n_input)]
        x_context_right = [word_to_vec(str(content[i]), word_to_vec_map) for i in range(cursor+n_input+1, cursor + 2 * n_input+1)]

        x_demo_context_left = [content[i] for i in range(cursor, cursor + n_input)]
        x_demo_context_right = [content[i] for i in range(cursor+n_input+1, cursor + 2 * n_input+1)]
        x_demo_context = (x_demo_context_left, x_demo_context_right)

        x_context = x_context_left + [np.zeros(vec_dim)] + x_context_right
        X.append(x_context)
        X_demo_context.append(x_demo_context)

        y_onehot = np.zeros([vocab_size], dtype=float)
        y_onehot[word_ind[str(content[cursor + n_input])]] = 1.0  # next word
        # y_vec = word_to_vec_map [str(content[cursor + n_input])]

        Y.append(y_onehot)
        cursor += 1

    X= np.array(X)
    X= np.reshape(X, [X.shape[0], X.shape[1], vec_dim])
    Y = np.array(Y)

    print('Training set len: {:,}'.format(X.shape[0]))

    return X, Y, word_to_vec_map, word_ind, ind_word, X_demo_context



def read_glove_vecs(glove_file):
    with open(glove_file, 'r') as f:
        words = set()
        word_to_vec_map = {}

        for line in f:
            line = line.strip().split()
            curr_word = line[0]
            words.add(curr_word)
            word_to_vec_map[curr_word] = np.array(line[1:], dtype=np.float32)

    return words, word_to_vec_map


def build_dataset(words):
    '''words -  the list of words '''
    count = collections.Counter(words).most_common()
    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return dictionary, reverse_dictionary


def initialize_parameters(n_hidden, vocab_size):
    """
    Initialize parameters with small random values

    """

    # Note: configure the output layer whereas the parameters of rnn are declared automatically
    # W_fw = tf.get_variable("W_fw", [n_hidden, vocab_size], initializer=tf.contrib.layers.xavier_initializer(seed=1))
    # b_fw = tf.get_variable("b_fw", [vocab_size], initializer=tf.zeros_initializer())
    #
    # W_bw = tf.get_variable("W_bw", [n_hidden, vocab_size], initializer=tf.contrib.layers.xavier_initializer(seed=1))
    # b_bw = tf.get_variable("b_bw", [vocab_size], initializer=tf.zeros_initializer())
    #
    # parameters = {"W_fw": W_fw, "b_fw": b_fw, "W_bw": W_bw, "b_bw": b_bw}
    W = tf.get_variable("W", [n_hidden *2 , vocab_size], initializer=tf.contrib.layers.xavier_initializer(seed=1), dtype=tf.float64) # to use tf.nn.static_bidirectional_rnn which retutn outputs  - a length T list of outputs (one for each input), which are depth-concatenated forward and backward outputs
    b = tf.get_variable("b", [vocab_size], initializer=tf.zeros_initializer(), dtype=tf.float64)

    parameters = {"W": W, "b": b}

    return parameters


def create_placeholders(n_input, vec_dim, vocab_size):
    # X[batch_size, n_context]  - Note: it will be splitted by last dimension to pass to RNN - the value represented is the int number the number in vocab

    # Let's add 1 as the last dimension which means there is only 1 feature - you may use it to pass one hot vector or whatever features
    X = tf.placeholder(tf.float64, shape=[None, n_input * 2 +1, vec_dim], name='X')  # batch_size, context, vecror representation)

    # Y [batch_size, len_of_vocabulary]  - Note: we need to use it as one-hot vector to calculate the cross entropy cost
    Y = tf.placeholder(tf.float64, shape=[None, vocab_size] , name='Y')  #  batch_size, vecror representation
    return (X,Y)


def forward_propagation(X, parameters, n_hidden, layers=2):

    # looks as batch size = 1

    print('\nShape of input =  {}'.format(X.shape))


    # Note: the following is required for static_bidirectional_rnn but not for stack_bidirectional_dynamic_rnn !!!
    # X = tf.split(X, (n_input* 2+ 1) , axis=-1)  #  default axis=0

    cells_fw = [tf.nn.rnn_cell.LSTMCell(num_units=n_hidden, state_is_tuple=True, dtype=tf.float64) for i in range(layers)]
    cells_bw = [tf.nn.rnn_cell.LSTMCell(num_units=n_hidden, state_is_tuple=True,dtype=tf.float64) for i in range(layers)]

    outputs, output_state_fw, output_state_bw = rnn.stack_bidirectional_dynamic_rnn(cells_fw= cells_fw, cells_bw= cells_bw, inputs= X, dtype=tf.float64) # returns a tuple (outputs, output_state_fw, output_state_bw) where: * outputs: Output Tensor shaped: batch_size, max_time, layers_output]. Where layers_output are depth-concatenated forward and backward outputs.

    # tf.add(tf.matmul(outputs[:, n_input+1], parameters['W']), parameters['b'])

    Z = tf.nn.softmax(tf.add(tf.matmul(outputs[:, n_input+1], parameters['W']), parameters['b']),         name='Z')
    print('\nOutput of RNN ("Z") =  {}'.format(Z))
    return Z


def compute_cost(Z, Y):
    """
    Returns:
    cost - Tensor of the cost function
        e.g. value 2.2901573
    """
    # Make sure sum over rows = 1
    # labels = np.array(Y, dtype=np.float32)
    # labels_sum = tf.reduce_sum(labels, axis=-1)
    # print(labels_sum)


    cost_cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=Z, labels=Y))
    # Defaulted  axis is -1 which is the last dimension.
    # each row of labels[i] must be a valid probability distribution

    return cost_cross_entropy



def random_mini_batches(X, Y,  m, mini_batch_size=64, seed=0):
    """
    Creates a list of random minibatches from (X, Y)

    Arguments:
    X -- input data, of shape (number of examples, n_context)
    Y -- true "label" , of shape (number of examples, vocab_size )
    mini_batch_size - size of the mini-batches, integer
    seed -- this is only for the purpose of grading, so that you're "random minibatches are the same as ours.

    Returns:
    mini_batches -- list of synchronous (mini_batch_X, mini_batch_Y)
    """

    mini_batches = []
    np.random.seed(seed)

    # Step 1: Shuffle (X, Y)
    permutation = list(np.random.permutation(m))
    shuffled_X = X[permutation, : , :]
    shuffled_Y = Y[permutation, :] #.reshape((Y.shape[0], m))

    # Step 2: Partition (shuffled_X, shuffled_Y). Minus the end case.
    num_complete_minibatches = math.floor(
        m / mini_batch_size)  # number of mini batches of size mini_batch_size in your partitionning
    for k in range(0, num_complete_minibatches):
        mini_batch_X = shuffled_X[k * mini_batch_size: k * mini_batch_size + mini_batch_size, :]
        mini_batch_Y = shuffled_Y[k * mini_batch_size: k * mini_batch_size + mini_batch_size:, :]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)

    # Handling the end case (last mini-batch < mini_batch_size)
    if m % mini_batch_size != 0:
        mini_batch_X = shuffled_X[num_complete_minibatches * mini_batch_size: m, :]
        mini_batch_Y = shuffled_Y[num_complete_minibatches * mini_batch_size: m, :]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)

    return mini_batches


def predict_word_by_vec(pred, ind_word, word_to_vec_map, n_top_to_consider=1):
    '''
    Сomputes the vec considering n_top of max values and their vectors representation
    :param pred:  output of softmax layer (evaluated) - distributed probabilities of predicted label
    :param ind_word: mapping of words of training corpus
    :param word_to_vec_map:
    :param n_top_to_consider:
    :return: word from pretrained wordembeddings which is the closest to computed vector
    '''


    # these are indices of vocab
    pred= pred.ravel()
    # pred_top_indices = pred.argsort(axis=-1).ravel()[-n_top_to_consider:]

    pred_top_indices = pred.argsort()[-n_top_to_consider:][::-1] # [::-1] - means reversed (optional in this case)

    # print the argmax word
    print('\nargmax word prediction: {}'.format(ind_word[pred_top_indices[0]]))
    # print('argmax word to vec: {}'.format(word_to_vec(str(ind_word[pred_top_indices[0]]) ,word_to_vec_map)))
    resulting_vec = np.sum([pred[i] * word_to_vec(str(ind_word[i]), word_to_vec_map).reshape(1,-1) for i in pred_top_indices], axis=0) / np.sum ([pred[i] for i in pred_top_indices])

    # print closest word to resulting vector:
    best_word, max_cosine_sim = get_closest_word(resulting_vec, word_to_vec_map)
    print('\tClosest word to top {} predictions vectors: {} [{}]'.format(n_top_to_consider, best_word, max_cosine_sim) )
    print ('\tConsidered top words: {}'.format([str(ind_word[i]) for i in pred_top_indices]))
    print('\tWith corresponding probabilities: {}'.format([pred[i] for i in pred_top_indices]))

    return best_word, pred_top_indices[0]


def word_to_vec(w , word_to_vec_map):
    if w not in word_to_vec_map:
        w ='unk'
    return word_to_vec_map[w]


def get_closest_word(vec, word_to_vec_map):
    words = word_to_vec_map.keys()
    max_cosine_sim = -100  # Initialize max_cosine_sim to a large negative number
    best_word = None  # Initialize best_word with None, it will help keep track of the word to output

    # loop over the whole word vector set
    for w in words:
        cosine_sim = cosine_similarity(vec.ravel(), word_to_vec_map[w])
        if cosine_sim > max_cosine_sim:
            max_cosine_sim = cosine_sim
            best_word = w
    # print ('\tComputing closest word to vec: {}[{}]'.format(best_word,max_cosine_sim))
    return best_word, max_cosine_sim


def cosine_similarity(u, v):
    """
    Cosine similarity reflects the degree of similariy between u and v

    Arguments:
        u -- a word vector of shape (n,)
        v -- a word vector of shape (n,)

    Returns:
        cosine_similarity -- the cosine similarity between u and v defined by the formula above.
    """
    # Compute the dot product between u and v
    dot = np.dot(u, v)
    # Compute the L2 norm of u  and v
    norm_u = np.linalg.norm(u)
    norm_v = np.linalg.norm(v)
    cosine_similarity = dot / (norm_u * norm_v)

    return cosine_similarity


def model(data, save_model_path, learning_rate=0.001, n_input = 3, n_hidden=512, num_epochs = 500, minibatch_size = 32, layers= 2):
    '''
    :param data:
    :param learning_rate:
    :param n_input: int - len of context
    :param n_hidden: int - number of units in RNN cell
    :param training_iters: int
    :param display_step: int
    :param save_model_path: str
    :return:
    '''

    tf.reset_default_graph()  # to be able to rerun the model without overwriting tf variables - actually I dont understand what it is for

    X_train, Y_train, word_to_vec_map, word_ind, ind_word, X_demo_context  = data
    vec_dim = X_train.shape[-1]
    m, vocab_size = Y_train.shape # len(word_ind)


    # Initialize parameters
    parameters = initialize_parameters(n_hidden, vocab_size=vocab_size)

    # Create Placeholders of shape (n_x, n_y)
    X, Y= create_placeholders(n_input, vec_dim, vocab_size)

    # Forward propagation: Build the forward propagation in the tensorflow graph
    Z = forward_propagation(X, parameters,n_hidden, layers)

    cost = compute_cost(Z, Y)

    # costs = []  # To keep track of the cost

    # Define the tensorflow optimizer. Use an AdamOptimizer.
    optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(cost)


    # predict_vec(Z, ind_word, word_to_vec_map, n_top_to_consider= 1)

    # Model evaluation
    correct_pred = tf.equal(tf.argmax(Z, -1), tf.argmax(Y, -1))
    # accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32), name = 'Acc')


    # use to save model
    saver = tf.train.Saver()
    # tf.add_to_collection('Z', Z)

    # Initialize all the variables
    init = tf.global_variables_initializer()
    seed = 3  # to keep consistent results
    costs = []  # To keep track of the cost

    print('\nTrainable variables: ')
    with tf.Session() as sess:
        sess.run(init)
        vars = tf.trainable_variables()
        for var in vars:
            print(var)

    # Start the session to compute the tensorflow graph
    print('\nTraining model...')
    with tf.Session() as sess:
        # Run the initialization
        sess.run(init)

        # Do the training loop
        for epoch in range(1,num_epochs+1): # to avoid print at first iter
            epoch_cost = 0.  # Defines a cost related to an epoch
            acc_epoch = 0
            # acc_troubleshooting = []
            num_minibatches = math.ceil(m / minibatch_size)

            seed = seed + 1
            minibatches = random_mini_batches(X_train, Y_train, m, minibatch_size, seed)

            for minibatch in minibatches:
                # Select a minibatch
                (minibatch_X, minibatch_Y) = minibatch

                # IMPORTANT: The line that runs the graph on a minibatch.
                # Run the session to execute the "optimizer" and the "cost", the feedict should contain a minibatch for (X,Y).
                _, acc, minibatch_cost = sess.run([optimizer, accuracy, cost], feed_dict={X: minibatch_X, Y: minibatch_Y})
                # acc_troubleshooting.append(acc)
                epoch_cost += minibatch_cost / num_minibatches

                acc_epoch += acc / num_minibatches


            # Print the cost every epoch
            if  epoch % 2 == 0:
                print("\n===\nCost after epoch %i: %f" % (epoch, epoch_cost))
                print("Average Accuracy (during performance of last epoch)= {:.1%}".format(acc_epoch))
                # print (acc_troubleshooting)


                general_accuracy = accuracy.eval({X: X_train, Y: Y_train})
                print("General Accuracy: {:.1%}".format (general_accuracy))


                # print_predictions(sess, X, Z, X_train, Y_train, word_to_vec_map, X_demo_context , ind_word, n_input, number=50)


            if epoch % 5 == 0:
                costs.append(epoch_cost)


        print("Optimization Finished!")

        # Save the variables to disk.
        save_path = saver.save(sess, save_model_path)
        print("Model saved in path: {}".format(save_path))

        print ('\nCheck at saving')
        print(sess.run('W:0'))

def print_predictions(sess, X, Z, X_train, Y_train,word_to_vec_map,  X_demo_context , ind_word, n_input, start=0, number = None):
    accuracy=[]
    cursor = start
    if not number:
        end = len(X_train)
    else:
        end = min(len(X_train), number + start)  # to cover al
    print('\nDemonstrate the predictions for contexts in range ({}, {}):'.format(start, end))
    while cursor < end:
        # x_context = [word_ind[str(content[i])] for i in range(cursor, cursor + n_input)]
        # x_context = np.reshape(x_context, [1, -1])

        x_batch = X_train[cursor].reshape(1,n_input*2+1, -1)
        pred = sess.run(Z, feed_dict={X: x_batch}) # this returns shape : (1, 113) [batch_size, vocab_size]

        pred_word, pred_index= predict_word_by_vec (pred, ind_word, word_to_vec_map, n_top_to_consider=5)

        true_index = np.argmax(Y_train[cursor])
        true_word = ind_word[true_index]

        fw_words = ' '.join(X_demo_context[cursor][0])
        bw_words = ' '.join(X_demo_context[cursor][1])

        print('{} -> {} [{}]  <-  {}'.format(fw_words, pred_word, true_word, bw_words))
        cursor += 1
        accuracy.append(pred_index == true_index)
    print('\nFactual Accuracy for range of {} contexts = {:.1%}'.format(number, np.average(accuracy)))



def generate_sample_index(pred_tf):
    '''
    :param pred is tensor
    :return: tf - index of sampled word
    Note : here is all operations ared performed with tensors not with np objects
    need to eval to get int value
    '''

    # sampling due to probability distribution
    pred_tf = tf.squeeze(pred_tf) # to exclude dimensionals of 1 - could be resolved by reshape
    dist = tf.distributions.Multinomial(total_count=1., probs = pred_tf)  # response with larger dimension - first dumension is dedicated for number of samples
    # here could be provided the ndarray or list e.g.   p = [.2, .3, .5]

    sample_tf = dist.sample(1)
    return tf.argmax(sample_tf, axis=1)[0] # since the sample_tf is tf of size 2 tf.argmax returns the array of size 1 - to get value we take first element



def use_trained_model(save_model_path, data):
    tf.reset_default_graph()

    X_train, Y_train, word_to_vec_map, word_ind, ind_word, x_demo_context  = data

    with tf.Session() as sess:

        # sess.run(init)
        save_graph_path =  save_model_path+ '.meta'
        saver = tf.train.import_meta_graph(save_graph_path)

        # Restore variables from disk.
        saver.restore(sess, save_model_path)
        print("\nModel restored.")

        graph = tf.get_default_graph()
        X = graph.get_tensor_by_name("X:0")

        # print('\nCheck at restoring')
        # print(sess.run(W))

        Z = graph.get_tensor_by_name("Z:0")

        Y = graph.get_tensor_by_name("Y:0")
        accuracy=  graph.get_tensor_by_name("Acc:0")

        general_accuracy = accuracy.eval({X: X_train, Y: Y_train})
        print("General Accuracy: {:.1%}".format(general_accuracy))


        #  lets predict for every context
        print_predictions(sess, X, Z, X_train, Y_train, word_to_vec_map, x_demo_context , ind_word, n_input , number=None)

        # is_continue  =  True
        #
        #
        # while is_continue:
        #     prompt = "\nProvide {} words (2 before, any target and 2 after the target word): ".format(2* n_input+1)
        #     sentence = input(prompt)
        #     sentence = sentence.strip()
        #     words = sentence.split(' ')
        #     if len(words) > n_input*2+1:
        #         words = words[:n_input*2+1]
        #         print('It will use first {} words: {}\n'.format(2*n_input+2, words))
        #
        #     provided_target = words[n_input]
        #     words[n_input] = '_target_'
        #
        #     x_batch = [word_ind[str(words[i])] for i in range(len(words))]
        #     x_batch = np.reshape(x_batch, [1, -1]) # make sure it is row
        #
        #     sample_ind_tf = generate_sample_index(Z)
        #
        #     sample_ind = sess.run(sample_ind_tf, feed_dict={X:x_batch})
        #     pred_index = int(tf.argmax(Z, axis = 1).eval(feed_dict={X: x_batch}))
        #     if pred_index!= sample_ind:
        #         print ('Sampling {} [argmax: {}]'.format( ind_word[sample_ind], ind_word[pred_index]))
        #     else:
        #         print('Sampling {}'.format(ind_word[sample_ind]))
        #

            # fw_words = ' '.join(words[: n_input])
            # bw_words = ' '.join(words[n_input + 1 :])
            #
            # print('{} -> {} [{}] <-  {}'.format(fw_words, ind_word[pred_index], provided_target, bw_words))


n_input = 3
n_hidden = 128
minibatch_size=1
layers= 2
num_epochs = 60

is_to_train = 0

training_corpus_file_name = '/Users/new/main_python_git/main_python_bitbucket/deep_learning/datasets/belling_the_cat.txt'
pre_trained_vec_file_name= '/Users/new/main_python_git/main_python_bitbucket/deep_learning/learning_gensim/pretrained_models/glove.6B/glove.6B.50d.txt'
save_model_path = "/Users/new/main_python_git/main_python_bitbucket/deep_learning/datasets/model_with_word_emb_181002.ckpt"


data = load_data(training_corpus_file_name, pre_trained_vec_file_name)



if is_to_train:
    model(data, save_model_path=save_model_path, learning_rate=0.001, n_input=n_input, n_hidden=n_hidden,
          num_epochs=num_epochs, minibatch_size=minibatch_size, layers=layers)
use_trained_model(save_model_path, data)



# === )))
# Cost after epoch 54: 3.745658
# Average Accuracy (during performance of last epoch)= 99.5%
# General Accuracy: 100.0%