"""categorization URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^sl/', include('sl_site.urls')), # site
    url(r'^api/', include('api.urls')), # main api
    url(r'^additional/', include('additional_api.urls')), # custom api views

]


# Available urls:
# nested===============================
# http://127.0.0.1:8000/api/accounts/
# http://127.0.0.1:8000/api/configurations/
# http://127.0.0.1:8000/api/accounts/5/
# http://127.0.0.1:8000/api/accounts/1/categories/
# http://127.0.0.1:8000/api/accounts/1/categories/2/
# http://127.0.0.1:8000/api/accounts/1/categories/2/subcategories/
# http://127.0.0.1:8000/api/accounts/1/rootcategories/
# http://127.0.0.1:8000/api/accounts/1/categories/5/keywords/

# http://127.0.0.1:8000/api/accounts/1/categories/5/keywords/2/
# the same as
# http://127.0.0.1:8000/api/accounts/1/keywords/2/

# http://127.0.0.1:8000/api/configurations/
# http://127.0.0.1:8000/api/configurations/96/

# just some action ===============================
# http://127.0.0.1:8000/api/change_account/5
# http://127.0.0.1:8000/api/active_account/




# custom (obsolete - covered by nested) ==================
# http://127.0.0.1:8000/additional/cat_keywords/
# http://127.0.0.1:8000/additional/cat_keywords/5/
# http://127.0.0.1:8000/additional/subcategories/ - does not work - obsolete


# site =======================================
# http://127.0.0.1:8000/sl/
# http://127.0.0.1:8000/sl/manage_accounts