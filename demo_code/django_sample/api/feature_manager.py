'''
this contains methods for recognizing features using local files
'''

from .models import Attribute, Keyword, Category
import pandas as pd
import os.path
import re
# import nltk
import json
import api.model_instance_manager as mim

def create_category_by_features(keywords, category_id, feature, criteria):
    if criteria != 'Is not empty':
        return 'This criteria is not supported.'

    df = get_dictionary(feature)
    dictionaries_names_list = get_feature_dictionaries_names(feature)
    get_features(df, dictionaries_names_list, keywords, feature)
    if criteria == 'Is not empty':
        build_category_of_non_empty(keywords,category_id, feature)

    return 'Operation completed'


def get_feature_dictionaries_names(feature):
    if feature== 'Brand':
        return ['Soft Drinks 25 2018', 'Soft Drinks 25 2017', 'Soft Drinks 25 2016', 'Food 50 2018', 'Food 50 2017', 'Food 50 2016', 'Football 50 2018', 'Cosmetics 50 2018', 'Airlines 50 2018', 'Cosmetics 50 2017', 'Cosmetics 50 2016', 'Exchanges 10 2018', 'Tyres 10 2018', 'Apparel 50 2018', 'Apparel 50 2017', 'Apparel 50 2016', 'Auto Components 10 2018', 'Auto 100 2018', 'Auto 100 2017', 'Auto 100 2016', 'Auto 100 2015', 'Car Rental Services 10 2018', 'Car Rental Services 10 2017', 'Car Rental Services 10 2016', 'IT Services 15 2018', 'IT Services 2017', 'IT Services 15 2016', 'IT Services 10 2015', 'Telecoms 300 2018', 'Telecoms 500 2017', 'Telecoms 500 2016', 'Logistics 25 2018', 'Logistics 25 2017', 'Logistics 25 2016', 'Insurance 100 2018', 'Insurance 100 2017', 'Insurance 100 2016', 'Telecoms Infrastructure 10 2018', 'Telecoms Infrastructure 10 2017', 'Telecoms Infrastructure 10 2016', 'Toys 25 2018', 'Toys 25 2017', 'Toys 25 2016', 'Global 500 2018', 'Global 500 2017', 'Global 500 2017.1', 'Global 500 2015', 'Global 500 2014', 'Global 500 2013', 'Global 500 2012', 'Global 500 2011', 'Global 500 2010', 'Global 500 2009', 'Restaurants 25 2018', 'Restaurants 25 2017', 'Restaurants 25 2016', 'Media 25 2018', 'Media 25 2017', 'Media 25 2016', 'Commercial Services 50 2018', 'Commercial Services 50 2017', 'Commercial Services 50 2016', 'Engineering and Construction 25 2018', 'Engineering and Construction 25 2017', 'Engineering and Construction 25 2016', 'Banking 500 2018', 'Banking 500 2017', 'Banking 500 2016', 'Hotels 50 2018', 'Hotels 50 2017', 'Hotels 50 2016', 'Utilities 50 2018', 'Utilities 50 2017', 'Utilities 50 2016', 'Mining, Iron & Steel 25 2017', 'Mining, Iron & Steel 25 2018', 'Mining, Iron & Steel 25 2016', 'Tech 100 2017', 'Tech 100 2016', 'Chemicals 10 2017', 'Chemicals 10 2016', 'US 500 2018', 'US 500 2017', 'Denmark 50 2018', 'Denmark 50 2017', 'Turkey 100 2018', 'Turkey 100 2017', 'Mexico 50 2018', 'Mexico 50 2017', 'Canada 100 2018', 'Canada 100 2017', 'Belgium 10 2018', 'Belgium 10 2017', 'Indonesia 100 2018', 'Indonesia 100 2017', 'Japan 50 2018', 'Japan 50 2017', 'South Africa 50 2018', 'South Africa 50 2017', 'Germany 100 2018', 'Germany 50 2017', 'Finland 10 2018', 'Finland 10 2017', 'UK 150 2018', 'UK 150 2017', 'Spain 100 2018', 'Spain 100 2017', 'Middle East 50 2018', 'Middle East 50 2017', 'Sri Lanka 100 2018', 'Sri Lanka 100 2017', 'Italy 50 2018', 'Italy 50 2017', 'Ireland 10 2018', 'Ireland 10 2017', 'China 300 2018', 'China 100 2017', 'Australia 100 2018', 'Australia 100 2017', 'Fortune-500', 'Most Valuable Brands 2018']
    if feature == 'Color':
        return ['Name', 'Hex']
    if feature == 'Fabric':
        return ['Source_1', 'Source_2', 'Source_3']


def get_dictionary(feature):
    file_name = '{}s_collection.xlsx'.format(feature.lower())
    root_path = os.path.dirname(__file__)
    full_path = os.path.join(os.path.join(root_path, 'feature_data_sources'), file_name)
    print('Reading file: {}...'.format(full_path))
    df = pd.read_excel(full_path)
    print('list of dictionaries: {}'.format(list(df)))
    return df


def get_features(df, columns_names, keywords, feature):
    '''
    :param keywords: list of Keyword instances
    :return: None
    '''
    print('Getting features of type: {}...'.format(feature))
    total_len= len(keywords)
    progress = 0
    for keyword in keywords:
        features_qs = Attribute.objects.filter(keyword=keyword.id, name= feature.lower())
        # if True: #len(features_qs) == 0:
        if len(features_qs) == 0:
            results = check_feature(keyword.phrase, df, columns_names,feature )
            if results:
                for result in results:
                    create_attribute_for_feature(feature, keyword, result)

        progress +=1
        print ('Progress: {:,} of {:,}'.format(progress, total_len))

    print('Getting features complete.')
    return


def check_feature(target_phrase, df, columns_names, feature):
    tokens = tokenize_sentence(target_phrase)
    tokenized_sentence_ngrams = get_ngrams_tokens(tokens, ngrams=4)
    results = []
    for candidate in tokenized_sentence_ngrams:
        for col in columns_names:
            bool_matrix = df[col].str.lower() == candidate.lower()
            # pattern= '\\b{}\\b'.format(candidate)
            # bool_matrix= df[col].str.match(pattern, case=False).fillna(False)
            if len(df[bool_matrix])>0:
                if feature== 'Color':
                    res = df[bool_matrix].iloc[0]
                    print('Detected color for "{}" in {}'.format(candidate, target_phrase))
                    if res['Name'] not in results:
                        res_dict = {'Color': res['Name'], 'Hex':res['Hex'], 'RGB':res['RGB']}
                        print(res_dict)
                        results.append(res_dict)
                elif col not in results:
                    print('Detected {}  "{}" in "{}"'.format(feature, candidate, target_phrase))
                    results.append({col: candidate})
                    break # don't show in other columns

    return results

def create_attribute_for_feature(feature, keyword, detected_source):
    keyword = Keyword.objects.get(id=keyword.id)
    attribute = Attribute(name=feature.lower(), keyword=keyword, value=json.dumps(detected_source))
    attribute.save()
    return


def build_category_of_non_empty(keywords,category_id, feature):
    print ('Building category of non empty "{}"...'.format(feature))
    all_detected_features= {}
    new_category= create_container_category(category_id, feature)
    for keyword in keywords:
        feature_qs = Attribute.objects.filter(keyword=keyword.id, name=feature.lower())
        if len(feature_qs) > 0:
            mim.assign_keyword_to_categories([keyword], [new_category])

    print('Building category of non empty "{}" complete'.format(feature))



def create_container_category(category_id, feature):
    origin_category_name = Category.objects.get(id=category_id).name
    origin_category_account = Category.objects.get(id=category_id).account
    new_category_name = 'Non empty "{}" from {}'.format(feature, origin_category_name)
    description = 'Created automatically for all keywords that has "{}" not empty'.format(feature)
    new_category = mim.create_category(new_category_name, origin_category_account, description=description)
    return new_category

def tokenize_sentence(target_str):
    target_str = re.sub(r"[_-]", ' ', target_str)
    re_pattern = r"[\w]+"
    tokens = re.findall(re_pattern, target_str)
    return [token for token in tokens if len(token) > 2 and re.search('\\D*', token)]

def get_ngrams_tokens(tokens, ngrams):
    '''
    :param tokens: list of str like   ['VP', 'engineering', 'for', 'adtech', 'companies', 'in', 'San', 'Francisco']
    :return: original list extended by ngrams e.g. {'San-Francisco', 'San Francisco', 'companies-in', 'adtech', 'VP-engineering',
             'in San', 'for', 'for adtech', 'companies', 'companies in', 'engineering for', 'for-adtech', 'adtech-companies', 'in-San',
            'adtech companies', 'in', 'engineering-for', 'VP', 'San', 'engineering', 'VP engineering', 'Francisco'}
    '''
    return list(set(build_ngrams_sign(tokens, ngrams)
                + build_ngrams_sign(tokens,ngrams, sign='-')
                + build_ngrams_sign(tokens,ngrams, sign='')))

def build_ngrams_sign(tokens, ngrams=3, sign=' '):
    return [sign.join(tokens[i:i + ngram + 1])
                for i in range(len(tokens))
                for ngram in range(ngrams)
                if i + ngram < len(tokens)]

