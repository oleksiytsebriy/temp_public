import json
from api.models import Account, Category, Keyword, Attribute, Configuration


def create_account(account_name):
    account = Account(name=account_name)
    account.save()
    return account


def create_category(category_name, account, parent_category = None, description = ''):
    category = Category(name=category_name, parent=parent_category, description=description, account=account)
    category.save()
    return category

    # cat_keywords = [] ===============================
    #
    # df[['Keyword']].apply(create_keyword, axis=1, account = account, cat_keywords = cat_keywords)
    # # Update Category =====
    #
    # category.keyword.set(cat_keywords)
    # category.save() ===============================

    # configuration = Configuration(active_account=account)
    # configuration.save()

def create_keyword(phrase, account):
    keyword = Keyword(phrase=phrase, account=account)
    keyword.save()
    return keyword


def assign_keyword_to_categories(keywords, categories):
    '''
    :param keywords: List of Keywords instance
    :param categories: list of categories instances
    :return: None
    '''
    for category in categories:
        keywords_list = list(category.keyword.all())
        for keyword in keywords:
            keywords_list.append(keyword)
            category.keyword.set(keywords_list)
    return

def create_attribute(name, keyword, type, value= None, public = None):
    attribute = Attribute(name=name, keyword=keyword, type=type, value=value)
    attribute.save()
    return attribute


def create_configuration(active_account):
    configuration = Configuration(active_account=active_account)
    configuration.save()
    return configuration
