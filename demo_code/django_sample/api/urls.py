from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

#
# # Create a router and register our viewsets with it.
# router = DefaultRouter()
# router.register(r'category', views.CategoryViewSet)
# router.register(r'account', views.AccountViewSet)
# router.register(r'keyword', views.KeywordViewSet)
# router.register(r'attribute', views.AttributeViewSet)
# # router.register(r'users', views.UserViewSet)
#
# # The API URLs are now determined automatically by the router.
#
# urlpatterns = [
#     url(r'^', include(router.urls)),
#
# ]


from rest_framework_nested import routers
router = routers.SimpleRouter()
router.register(r'accounts', views.AccountViewSet)
router.register(r'configurations', views.ConfirgurationViewSet)

accounts_router = routers.NestedSimpleRouter(router, r'accounts', lookup='account')
accounts_router.register(r'keywords', views.AccountKeywordsViewSet, base_name='account-keywords')
accounts_router.register(r'categories', views.AccountCategoriesViewSet, base_name='account-categories')
accounts_router.register(r'rootcategories', views.AccountRootCategoriesViewSet, base_name='account-root-categories')


keywords_router = routers.NestedSimpleRouter(accounts_router, r'keywords', lookup='keyword')
keywords_router.register(r'attributes', views.AccountKeywordsAttributesViewSet, base_name='account-keyword-attributes')

categories_router = routers.NestedSimpleRouter(accounts_router, r'categories', lookup='category')
categories_router.register(r'keywords', views.AccountCategoryKeywordsViewSet, base_name='account-category-keywords')
categories_router.register(r'subcategories', views.AccountCategorySubcategoriesViewSet, base_name='account-category-subcategories')


from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(accounts_router.urls)),
    url(r'^', include(keywords_router.urls)),
    url(r'^', include(categories_router.urls)),
    url(r'^active_account/$', views.get_active_account, name='get-active-account'),
    url(r'^change_account/(?P<account_pk>[^/.]+)$', views.change_account, name='change-account'),
    url(r'^operation_js/$', views.apply_operation_js, name='apply_operation_js'),
    url(r'^operation/$', views.apply_operation, name='apply_operation'),
]


if settings.DEBUG: # this is to let save the files locally - think it could be removed since we don't  call that file then
    urlpatterns +=  static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns +=  static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns) # optional