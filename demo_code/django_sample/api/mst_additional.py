import io
from time import gmtime, strftime

def log(message, file_name, is_time_stamp= False):
    print (message)
    encoding = 'utf-8'

    with io.open(file_name, 'a', encoding=encoding) as f:
        if is_time_stamp:
            f.write('\n{}:'.format(strftime("%Y-%m-%d %H:%M:%S", gmtime())))
        output_message='{}\n'.format(message)
        f.write(output_message)