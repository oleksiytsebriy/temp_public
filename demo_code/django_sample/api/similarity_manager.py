'''
This is inherited from final version (demo 180429 - release of third sprint)  - clustering by similarity

This is based on using from scipy.cluster.hierarchy import complete
It is slioghtly modified due to integration into django project
which allows to build linkage tree
   Source file:  clustering_agglomerative_hierarchy.py

Notes:

    * scipy.cluster.hierarchy.complete  - the distance (between clusters) column a little strange  - not the exact distances

Updated by Oleksiy.Tsebriy 180709
'''

import pickle
import time
start_time = time.time()
from .models import Keyword, Category
import api.model_instance_manager as mim
import itertools
from scipy.cluster.hierarchy import complete #, dendrogram, average, linkage

# import matplotlib
# matplotlib.use('TkAgg') # use this for mac
# import matplotlib.pyplot as plt

from sklearn.feature_extraction.text import CountVectorizer

import os #, sys
# path_to_common_dir = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# print (path_to_common_dir )
# sys.path.append(path_to_common_dir)
# import miscelanous_tools as mst

import re
# from sklearn.cluster import AgglomerativeClustering

import numpy as np
import pandas as pd
from nltk.tokenize import word_tokenize
from nltk.corpus import wordnet as wn
import editdistance
from nltk.corpus import stopwords
# import math


class Similarity_Categorization():
    def __init__(self,keywords, origin_category_id, similarity_threshold, ngrams_limit = 4):
        self.keywords = keywords
        self.ngrams_limit= ngrams_limit
        self.origin_category_id = origin_category_id
        self.similarity_threshold = similarity_threshold

        self.get_cached_similarity_entities()
        self.stop_words = set(stopwords.words('english'))

    def get_cached_similarity_entities(self):
        root_path = os.path.dirname(__file__)
        filename= 'pickled_similarity_entities'
        self.pickled_entities_full_path = os.path.join(os.path.join(root_path, 'pickles'), filename)
        if os.path.isfile(self.pickled_entities_full_path):
            with open(self.pickled_entities_full_path, 'rb') as f:
                pickled_entities = pickle.load(f)

            self.cached_words_similarity = pickled_entities [0]
            self.cached_synsets_similarities = pickled_entities [1]
            self.cached_synsets = pickled_entities [2]
            self.len_cached_words_similarity = len(self.cached_words_similarity)
            self.len_cached_synsets_similarities = len(self.cached_synsets_similarities)
            self.len_cached_synsets= len(self.cached_synsets)
            print('Cached location results are found.\n'
                  'Len of cached words similarity = {:,}\n'
                  'Len of cached_synsets similarities= {:,}\n'
                  'Len of cached synsets = {:,}'.format(
                self.len_cached_words_similarity, self.len_cached_synsets_similarities, self.len_cached_synsets))
        else:
            print ('No cached location results are found')
            self.cached_words_similarity= {}
            self.cached_synsets_similarities= {}
            self.cached_synsets= {}
            self.len_cached_words_similarity= self.len_cached_synsets_similarities= self.len_cached_synsets = 0
        self.flag_to_save_pickled_entities = False


    def run_categorization_by_similarity(self): # main function
        '''
        :param keywords: list of Keywords instances to categorize
        :param origin_category_id: id of category keywords are selected from
        :return: None
        '''
        self.condensed_matrix = self.calc_condensed_matrix()
        self.update_cached_results()
        self.resulted_clusters = self.categorize_by_similarity()
        self.create_categories()

        return

    def create_categories(self): # todo for django
        for label in self.resulted_clusters:
            cluster_suggested_name = self.resulted_clusters[label][1]
            cluster_suggested_name = ' '.join(self.remove_stop_words(self.tokenize_phrase(cluster_suggested_name)))

            cluster_members = list(keywords[self.resulted_clusters[label][0]])
            if len(cluster_members) == 1:
                print('[keyword] {}'.format(cluster_suggested_name))
            else:
                print('[cluster] "{}" / {:,} keywords / tags: {}'.format(cluster_suggested_name, len(cluster_members),
                                                                         get_frequent_words(cluster_members)))
                print('\t{}\n'.format(cluster_members))

    def get_frequent_words(self, top_number=3):
        vect = CountVectorizer().fit(self.keywords) # todo convert to strings
        features = vect.get_feature_names()
        all_words = ' '.join(self.keywords)# todo convert to strings
        features_count_dict = {feature: all_words.count(feature) for feature in features if len(feature) > 2}
        if len(features_count_dict) < top_number:
            return sorted(features_count_dict, key=features_count_dict.get, reverse=True)
        return sorted(features_count_dict, key=features_count_dict.get, reverse=True)[:top_number]


    def categorize_by_similarity(self):
        print('similarity_threshold = {}%'.format(self.similarity_threshold * 100))
        distance_threshold = self.convert_siliratity_to_distance()
        linkage_matrix = complete(condensed_matrix)  # this represents the max distance measure  - consider the fareest in the cluster
        # linkage_matrix = average(upper_triangular_matrix) # alternative measure

        # init clusters with every keyword in keywords
        resulted_clusters = {i: ([i], self.keywords[i]) for i in
                             range(len(self.keywords))}  # every cluster is label/int: ([]/list, main_keyword/Keyword))???

        # Run clustering
        continue_clustering = True
        step_number = 0
        while continue_clustering:
            clusters_to_merge = (linkage_matrix[step_number, 0], linkage_matrix[step_number, 1])
            excpected_total = linkage_matrix[step_number, 3]
            next_distance = linkage_matrix[step_number, 2]
            new_label = step_number + len(keywords)

            if next_distance < distance_threshold:
                resulted_clusters = self.merge_clusters(resulted_clusters, clusters_to_merge, new_label, excpected_total)
                step_number += 1
                if step_number >= len(linkage_matrix):
                    continue_clustering = False
            else:
                continue_clustering = False

        # print ('results: {}'.format(resulted_clusters))
        outliers = [label for label in resulted_clusters if len(resulted_clusters[label][0]) == 1]
        print('\n= Results =')
        print('Clusters number: {:,}'.format(len(resulted_clusters) - len(outliers)))
        print('Outliers number: {:,}'.format(len(outliers)))

        return resulted_clusters

    def update_cached_results(self):
        if self.flag_to_save_pickled_locations:
            pickled_lresults = [self.cached_words_similarity ,
                                 self.cached_synsets_similarities,
                                 self.cached_synsets]
            with open(self.pickled_entities_full_path, "wb") as f:
                pickle.dump(pickled_lresults, f, pickle.HIGHEST_PROTOCOL)
            print('Cached similarity results are updated. \n'
                  'Len of new stored words similarity = {:,}\n'
                  'Len of new stored synsets similarities = {:,}\n'
                  'Len of new stored synsets = {:,}'.format(
                (len(self.cached_words_similarity)-self.len_cached_words_similarity) ,
                (len(self.cached_synsets_similarities)-self.len_cached_synsets_similarities),
                (len(self.cached_synsets) - self.len_cached_synsets)))
        else:
            print('Cached similarity results are not updated due to no new data obtained')

    def calc_condensed_matrix(self):
        '''
        Computes the condensed distance matrix
        e.g. for 3 elements:  [dist(0,1), dist(0,2),dist(1,2)]
        e.g. for 4 elements: [dist(0,1), dist(0,2), dist(0,3), dist(1,2), dist(1,3), dist(2,3)]
        '''
        print('\nCalculating distance matrix...')

        len_keywords= len(self.keywords)
        len_of_condensed_matrix= len(list(itertools.combinations(range(len_keywords), 2)))
        condensed_matrix= np.zeros(len_of_condensed_matrix)
        all_combinations= list(itertools.combinations(range(len_keywords), 2))
        last_progress= 0
        for i in range(len_of_condensed_matrix):
            current_progress = '{:.1%}'.format(i/len(all_combinations))
            if current_progress !=last_progress:
                print ('Progress = {:,} of {:,} ({:.1%})'.format(i, len(all_combinations), (i/len(all_combinations))))
                last_progress= current_progress
            pair_indeces= all_combinations[i]
            condensed_matrix[i] = convert_siliratity_to_distance(calc_phrase_similarity(self.keywords[pair_indeces[0]], self.keywords[pair_indeces[1]]))

        return condensed_matrix


    def calc_phrase_similarity(self, current_phrase, target_phrase):
        '''
        current_phrase - str - one of all phrases in the list
        target phrase is stored in self.target_words
        '''
        if len (current_phrase) == 0 or len(target_phrase)== 0:
            return 0

        target_phrase_words = self.tokenize_phrase(target_phrase)
        target_phrase_words= self.remove_stop_words(target_phrase_words)
        current_phrase_words = self.tokenize_phrase(current_phrase)
        current_phrase_words = self.remove_stop_words(current_phrase_words)

        tw_all_similarities= [self.target_word_similarity_score(current_phrase_words, tw) for tw in target_phrase_words]
        if len(tw_all_similarities)>0:
            tw_similarity= np.mean(tw_all_similarities)
        else:
            tw_similarity =  0

        cw_all_similarities= [self.target_word_similarity_score(target_phrase_words, cw) for cw in current_phrase_words]
        if len(cw_all_similarities)>0:
            cw_similarity= np.mean(cw_all_similarities)
        else:
            cw_similarity = 0

        return np.mean([tw_similarity, cw_similarity])

    def tokenize_phrase(self, phrase):
        tokens = word_tokenize(phrase)
        return [token for token in tokens]

    def remove_stop_words(self, words):
        words_to_remove = [w for w in words if
                           w in stop_words]
        return [w for w in words if w not in words_to_remove]

    def target_word_similarity_score(self, current_words, target_word):
        '''
        Calculates similarity between all current words (all synsets) and ONE target word (all synsets)
        in case word has no synsets the Levinshtein distance is calculated and score calculated as 0.8^Ld (1 diff - 0.8, 2 - 0.64, 3- 0.51, ...)
        :param current_words: list of words of current phrase
        :param target_word: str
        :return: float
        '''
        if len(current_words) == 0:
            return 0
        all_current_words_similarities = []
        for current_word in current_words:
            all_current_words_similarities.append(self.calc_memoized_words_similarity(current_word, target_word))
        if len(all_current_words_similarities) == 0:
            return 0
        return max(all_current_words_similarities)

    def calc_memoized_words_similarity(self, word_a, word_b):
        '''wrapper on calc_words_similarity'''
        key_memo = '{}_{}'.format(word_a, word_b)
        if key_memo in self.cached_words_similarity:
            return self.cached_words_similarity[key_memo]
        key_memo_reversed = '{}_{}'.format(word_b, word_a)
        if key_memo_reversed in self.cached_words_similarity:
            return self.cached_words_similarity[key_memo_reversed]
        # The following is "else" cloud
        self.cached_words_similarity[key_memo] = self.calc_words_similarity(word_a, word_b)
        self.flag_to_save_pickled_entities= True
        return self.cached_words_similarity[key_memo]


    def calc_words_similarity(self, word_a, word_b):
        ''' Calculates wup similarity if synsets available else score based on edit distance
            in case of synsets available returns max of all values
        '''
        # print ('len(self.memoized_words_similarity)= {}'.format(len(self.memoized_words_similarity)))

        word_a_synsets = self.get_memoized_synsets(word_a)
        word_b_synsets = self.get_memoized_synsets(word_b)
        if len(word_a_synsets) == 0 or len(word_b_synsets) == 0:
            return self.calc_edit_score(word_a, word_b)
        else:
            similarities_all_synsets = []
            for word_a_synset in word_a_synsets:
                for word_b_synset in word_b_synsets:
                    if word_a_synset.pos() == word_b_synset.pos():  # this speeds up the calculation
                        synsets_similarity = self.calc_memoized_similarity(word_a_synset, word_b_synset)
                        if synsets_similarity:  # some time return None
                            similarities_all_synsets.append(synsets_similarity)
                        else:
                            similarities_all_synsets.append(0)
                    else:
                        similarities_all_synsets.append(0)
            return max(similarities_all_synsets)

    def get_memoized_synsets(self, word):
        if word not in self.cached_synsets:
            self.cached_synsets[word] = wn.synsets(word)
            self.flag_to_save_pickled_entities= True
        return self.cached_synsets[word]

    def calc_edit_score(self, w1, w2):
        '''calc the score being based on edit (Levinshtein) distance
            1 difference - 0.8, 2- 0.64, 3-.51 ...
        '''
        edit_dist = editdistance.eval(w1, w2)
        if edit_dist > len(w1) or edit_dist > len(w2):  # no common letter
            return 0
        return 0.8 ** edit_dist

    def calc_memoized_similarity(self, current_word_synset, target_word_synset):
        key_memo = '{}_{}'.format(current_word_synset.name(), target_word_synset.name())
        if key_memo in self.cached_synsets_similarities:
            return self.cached_synsets_similarities[key_memo]
        key_memo_reversed = '{}_{}'.format(target_word_synset.name(), current_word_synset.name())
        if key_memo_reversed in self.cached_synsets_similarities:
            return self.cached_synsets_similarities[key_memo_reversed]
        self.cached_synsets_similarities[key_memo] = current_word_synset.wup_similarity(target_word_synset)
        self.flag_to_save_pickled_entities= True
        return self.cached_synsets_similarities[key_memo]

    def convert_siliratity_to_distance(self):
        return 20 if self.similarity_threshold == 0 else abs( # 20 is to cover for sure
            np.log(self.similarity_threshold, 0.8))  # abs to avoid -0.0 but probably it's ok

    def merge_clusters(self, current_clusters, clusters_to_merge, new_label, expected_total):
        '''
        :param current_clusters: dict of pairs: label and (cluster+main_phrase)  =  (list of indices of keywords) e.g. {1: [23,5,5]}
            main phrase is the first keyword at creating cluster - lets consider it as core of cluster since it gets merged first - thus the closest to all rest
        :param clusters_to_merge: list of 2 labels of closest clusters - candidates to merge e.g. [34,57]
        :param new_label: int  - the label generated by scipy.cluster.hierarchy
        :param expected_total: optional  - to check the anount of new cluster after merge due to provided by scipy.cluster.hierarchy
        :return:
        '''
        new_list = current_clusters[clusters_to_merge[0]][0] + current_clusters[clusters_to_merge[1]][0]
        main_phrase = current_clusters[clusters_to_merge[0]][1]
        current_clusters[new_label] = (new_list, main_phrase)
        if len(current_clusters[new_label][0]) != expected_total:
            raise SystemExit('Error at merging clusters')

        del current_clusters[clusters_to_merge[0]]
        del current_clusters[clusters_to_merge[1]]

        return current_clusters


    def todo_create_categories(self, unique_location_level_entities):
        print('Creating categories in database...')
        all_keywords_tracker = {keyword.id: None for keyword in self.keywords}
        origin_category_name = Category.objects.get(id=self.origin_category_id).name
        origin_category_account = Category.objects.get(id=self.origin_category_id).account
        new_category_name = '{} categorized by {}'.format(origin_category_name, self.location_entity_name)
        description = 'Created automatically as container for location categories'
        container_category = mim.create_category(new_category_name, origin_category_account, description=description)

        for location_value in unique_location_level_entities:
            keywords_for_category = get_keywords_with_attribute(self.keywords, 'location', self.location_level, location_value)
            for keyword in keywords_for_category:  # mark the keyword is assigned to some category
                all_keywords_tracker[keyword.id] = True
            category_name = '"{}" {} category '.format(location_value, self.location_entity_name)
            description = 'Created automatically due to categorizing by location {}'.format(location_value)
            loc_category = mim.create_category(category_name, origin_category_account,
                                               parent_category=container_category, description=description)
            mim.assign_keyword_to_categories(keywords_for_category, [loc_category])
        for keyword_id in all_keywords_tracker:
            if not all_keywords_tracker[keyword_id]:
                keyword = Keyword.objects.get(id=keyword_id)
                mim.assign_keyword_to_categories([keyword], [container_category])



# def convert_siliratity_to_distance(phrase_similarity):
#     return 20 if phrase_similarity==0 else abs(np.log(phrase_similarity,0.8))  # abs to avoid -0.0 but probably it's ok


# def tokenize_phrase(phrase):
#     tokens= word_tokenize(phrase)

    # return [w for w in re.findall(r"[\w]+", phrase) if len (w)>1]  # looks no better then native word_tokenizer
    # return [token for token in tokens if len (token)>1]
    # return [token for token in tokens ] #if len (token)>1]

# def calc_phrase_similarity(current_phrase, target_phrase):
#     '''
#     current_phrase - str - one of all phrases in the list
#     target phrase is stored in self.target_words
#     '''
#     if len (current_phrase)== 0 or len(target_phrase)== 0:
#         return 0
#
#     # target_phrase_words = word_tokenize(target_phrase)
#     target_phrase_words = tokenize_phrase(target_phrase)
#     target_phrase_words= remove_stop_words(target_phrase_words)
#     # current_phrase_words = word_tokenize(current_phrase)
#     current_phrase_words = tokenize_phrase(current_phrase)
#     current_phrase_words = remove_stop_words(current_phrase_words)
#
#     tw_all_similarities= [target_word_similarity_score(current_phrase_words, tw) for tw in target_phrase_words]
#     if len(tw_all_similarities)>0:
#         tw_similarity= np.mean(tw_all_similarities)
#     else:
#         tw_similarity =  0
#
#     cw_all_similarities= [target_word_similarity_score(target_phrase_words, cw) for cw in current_phrase_words]
#     if len(cw_all_similarities)>0:
#         cw_similarity= np.mean(cw_all_similarities)
#     else:
#         cw_similarity = 0
#
#     return np.mean([tw_similarity, cw_similarity])


# def target_word_similarity_score(current_words, target_word):
#     '''
#     Calculates similarity between all current words (all synsets) and ONE target word (all synsets)
#     in case word has no synsets the Levinshtein distance is calculated and score calculated as 0.8^Ld (1 diff - 0.8, 2 - 0.64, 3- 0.51, ...)
#     :param current_words: list of words of current phrase
#     :param target_word: str
#     :return: float
#     '''
#     if len(current_words)==0:
#         return 0
#     all_current_words_similarities= []
#     for current_word in current_words:
#         all_current_words_similarities.append(calc_memoized_words_similarity(current_word, target_word))
#     if len(all_current_words_similarities)==0:
#         return 0
#     return max(all_current_words_similarities)

# def calc_memoized_words_similarity(word_a, word_b):
#     '''wrapper on calc_words_similarity'''
#     key_memo = '{}_{}'.format(word_a, word_b)
#     if key_memo in memoized_words_similarity:
#         return memoized_words_similarity[key_memo]
#     key_memo_reversed = '{}_{}'.format(word_b,word_a)
#     if key_memo_reversed in memoized_words_similarity:
#         return memoized_words_similarity[key_memo_reversed]
#     memoized_words_similarity[key_memo] = calc_words_similarity(word_a,word_b)
#     return memoized_words_similarity[key_memo]

# def calc_words_similarity(word_a,word_b):
#     ''' Calculates wup similarity if synsets available else score based on edit distance
#         in case of synsets available returns max of all values
#     '''
#     # print ('len(self.memoized_words_similarity)= {}'.format(len(self.memoized_words_similarity)))
#
#     word_a_synsets = get_memoized_synsets(word_a)
#     word_b_synsets = get_memoized_synsets(word_b)
#     if len (word_a_synsets)==0 or len (word_b_synsets)==0:
#        return calc_edit_score(word_a,word_b)
#     else:
#         similarities_all_synsets= []
#         for word_a_synset in word_a_synsets:
#             for word_b_synset in word_b_synsets:
#                 if word_a_synset.pos() == word_b_synset.pos():  # this speeds up the calculation
#                     synsets_similarity =  calc_memoized_similarity(word_a_synset, word_b_synset)
#                     if synsets_similarity: # some time return None
#                        similarities_all_synsets.append(synsets_similarity)
#                     else:
#                         similarities_all_synsets.append(0)
#                 else:
#                     similarities_all_synsets.append(0)
#         return max(similarities_all_synsets)

# def get_memoized_synsets(word):
#     if word not in memoized_synsets:
#         memoized_synsets[word]= wn.synsets(word)
#     return memoized_synsets[word]


# def calc_memoized_similarity (current_word_synset, target_word_synset):
#     key_memo = '{}_{}'.format(current_word_synset.name(),target_word_synset.name())
#     if key_memo in memoized_synsets_similarities:
#         return memoized_synsets_similarities[key_memo]
#     key_memo_reversed = '{}_{}'.format(target_word_synset.name(), current_word_synset.name())
#     if key_memo_reversed in memoized_synsets_similarities:
#         return memoized_synsets_similarities[key_memo_reversed]
#     memoized_synsets_similarities[key_memo] = current_word_synset.wup_similarity(target_word_synset)
#     return memoized_synsets_similarities[key_memo]

# def calc_edit_score(w1, w2):
#     '''calc the score being based on edit (Levinshtein) distance
#         1 difference - 0.8, 2- 0.64, 3-.51 ...
#     '''
#     edit_dist = editdistance.eval(w1, w2)
#     if edit_dist> len (w1) or edit_dist> len (w2): # no common letter
#         return 0
#     return 0.8 ** edit_dist
#

# def remove_stop_words(words):
#     words_to_remove = [w for w in words if w in stop_words]# or len(w)<3]  # # remove short words # Note: Decided to keep all tokens to avoid empty results
#     return [w for w in words if w not in words_to_remove]



# def calc_condensed_matrix(keywords_list):
#     '''
#     Computes the condensed distance matrix
#     e.g. for 3 elements:  [dist(0,1), dist(0,2),dist(1,2)]
#     e.g. for 4 elements: [dist(0,1), dist(0,2), dist(0,3), dist(1,2), dist(1,3), dist(2,3)]
#     '''
#
#     file_name = 'Pickles/distance_matrix_pickle_{}'.format(len(keywords_list))
#     if not mst.is_local_file_found(file_name):
#         len_keywords= len(keywords_list)
#         len_of_condensed_matrix= len(list(itertools.combinations(range(len_keywords), 2)))
#         condensed_matrix= np.zeros(len_of_condensed_matrix)
#         all_combinations= list(itertools.combinations(range(len_keywords), 2))
#         last_progress= 0
#         for i in range(len_of_condensed_matrix):
#             current_progress = '{:.1%}'.format(i/len(all_combinations))
#             if current_progress !=last_progress:
#                 print ('Progress = {:,} of {:,} ({:.1%})'.format(i, len(all_combinations), (i/len(all_combinations))))
#                 last_progress= current_progress
#             pair_indeces= all_combinations[i]
#             condensed_matrix[i] = convert_siliratity_to_distance(calc_phrase_similarity(keywords_list[pair_indeces[0]], keywords_list[pair_indeces[1]]))
#
#         with open(file_name, "wb") as f:  # write bytes mode
#             pickle.dump(condensed_matrix, f, pickle.HIGHEST_PROTOCOL)
#
#     else:
#         print ('Condensed distance matrix is precomputed already')
#         with open(file_name, 'rb') as f:
#             condensed_matrix = pickle.load(f)
#
#     return condensed_matrix
#
# def merge_clusters(current_clusters,clusters_to_merge, new_label, expected_total):
#     '''
#     :param current_clusters: dict of pairs: label and (cluster+main_phrase)  =  (list of indices of keywords) e.g. {1: [23,5,5]}
#         main phrase is the first keyword at creating cluster - lets consider it as core of cluster since it gets merged first - thus the closest to all rest
#     :param clusters_to_merge: list of 2 labels of closest clusters - candidates to merge e.g. [34,57]
#     :param new_label: int  - the label generated by scipy.cluster.hierarchy
#     :param expected_total: optional  - to check the anount of new cluster after merge due to provided by scipy.cluster.hierarchy
#     :return:
#     '''
#     new_list =current_clusters[clusters_to_merge[0]][0]+ current_clusters[clusters_to_merge[1]][0]
#     main_phrase = current_clusters[clusters_to_merge[0]][1]
#     current_clusters[new_label] = (new_list, main_phrase)
#     if len (current_clusters[new_label][0]) != expected_total:
#         raise SystemExit('Error at merging clusters')
#
#     del current_clusters[clusters_to_merge[0]]
#     del current_clusters[clusters_to_merge[1]]
#
#     # current_clusters[new_label] = current_clusters[clusters_to_merge[0]]+ current_clusters[clusters_to_merge[1]]
#     # if len (current_clusters[new_label] ) != expected_total:
#     #     print ('Error at merging clusters')
#     # del current_clusters[clusters_to_merge[0]]
#     # del current_clusters[clusters_to_merge[1]]
#
#     return current_clusters


# def get_frequent_words(keywords_list, top_number= 3):
#     # print (keywords_list)
#     vect = CountVectorizer().fit(keywords_list)
#     features = vect.get_feature_names()
#     all_words = ' '.join(keywords_list)
#     # words_count_dict = ({w: all_words.count(w) for w in re.findall(r"[\w]+", all_words) if len(w) > 2})
#     features_count_dict = {feature: all_words.count(feature) for feature in features if len(feature) > 2}
#     if len(features_count_dict)<top_number:
#         return sorted(features_count_dict , key=features_count_dict.get, reverse=True)
#     return sorted(features_count_dict , key=features_count_dict.get, reverse=True)[:top_number]



#
# print ('\n===Start Execution===')
# # is_debug_mode= True
#
# head_numbers = None
# # head_numbers= 40
#
# similarity_threshold= 0.87
# similarity_threshold= 0.75
# similarity_threshold= 0.60
# # similarity_threshold= 0.40
#
#
# input_file_name = '/Users/new/science/data/otsebriy/categorization_data/KeywordList_Nike_cut.xlsx'
# # input_file_name= '/Users/new/science/data/otsebriy/categorization_data/KeywordList_several_cat.xlsx'
# # input_file_name = '/Users/new/science/data/otsebriy/categorization_data/KeywordList_1_cat.xlsx'
#
# df_keywords = pd.read_excel(input_file_name, skiprows=11)  # adopted for current export file from searchlight
#
# if head_numbers:
#     keywords= df_keywords['Keyword'].head(head_numbers).values # use to test providing few keywords e.g. 6-12
# else:
#     keywords= df_keywords['Keyword'].values
#
#
#
# # print keywords:
# print ('\n = All keywords = ')
# for i in range(len(keywords)):
#     print ('{}: {}'.format(i,keywords[i]))
#
# # start_calculating_scores_time = time.time()
# #
# # stop_words = set(stopwords.words('english'))
# # memoized_words_similarity= {}
# # memoized_synsets_similarities= {}
# # memoized_synsets = {}
# #
#
# # print (convert_siliratity_to_distance(calc_phrase_similarity('air deflectors for vents',' air deflector')))
#
# #
# # print ('\n= Calculating distance matrix =')
# # start_calc_matrix= time.time()
# # condensed_matrix= calc_condensed_matrix(keywords)
# # # print ('\ncondensed_matrix: \n {}'.format(condensed_matrix)) # print to test in case low number of keywords e.g. 6
# #
# # print ('\n= Clustering =')
# # print ('similarity_threshold = {}%'.format(similarity_threshold*100))
# #
# # distance_threshold = convert_siliratity_to_distance(similarity_threshold)
# # start_clustering= time.time()
# # linkage_matrix = complete(condensed_matrix) # this represents the max distance measure  - consider the fareest in the cluster
# # # linkage_matrix = average(upper_triangular_matrix) # alternative measure
# # if is_debug_mode:
# #     print (linkage_matrix)
# #
# #
#
# # init clustesr with every keyword in keywords
# # resulted_clusters={i: ([i], keywords[i]) for i in range(len(keywords))} # every cluster is label/int: ([]/list, main_keyword/str))
#
# #
# # # Run clustering
# # continue_clustering= True
# # step_number = 0
# # while continue_clustering:
# #     clusters_to_merge = (linkage_matrix[step_number,0],linkage_matrix[step_number,1])
# #     excpected_total =  linkage_matrix[step_number,3]
# #     next_distance = linkage_matrix[step_number,2]
# #     new_label = step_number + len(keywords)
# #
# #     if next_distance < distance_threshold :
# #         resulted_clusters= merge_clusters(resulted_clusters, clusters_to_merge, new_label, excpected_total)
# #         step_number +=1
# #         if step_number>=len(linkage_matrix):
# #             continue_clustering = False
# #     else:
# #         continue_clustering= False
#
#
# # # print ('results: {}'.format(resulted_clusters))
# # outliers= [label for label in resulted_clusters if len(resulted_clusters[label][0])==1]
# # print ('\n= Results =')
# # print ('Clusters number: {:,}'.format(len(resulted_clusters)-len(outliers)))
# # print ('Outliers number: {:,}'.format(len(outliers)))
#
#
# # print members
# for label in resulted_clusters:
#     cluster_suggested_name= resulted_clusters[label][1]
#     cluster_suggested_name = ' '.join(remove_stop_words(tokenize_phrase(cluster_suggested_name)))
#
#     cluster_members= list(keywords[resulted_clusters[label][0]])
#     if len(cluster_members)==1:
#         print ('[keyword] {}'.format(cluster_suggested_name))
#     else:
#         print('[cluster] "{}" / {:,} keywords / tags: {}'.format(cluster_suggested_name, len(cluster_members), get_frequent_words(cluster_members)))
#         print ('\t{}\n'.format(cluster_members))
#
# print ()
# # print only main info
# for label in resulted_clusters:
#     # print ('\nCluster # {}: \n{}'.format(reset_labels_dict[label],list(keywords[resulted_clusters[label]])))
#     cluster_suggested_name= resulted_clusters[label][1]
#     cluster_suggested_name = ' '.join(remove_stop_words(tokenize_phrase(cluster_suggested_name)))
#
#     cluster_members= list(keywords[resulted_clusters[label][0]])
#     if len(cluster_members)==1:
#         print ('[keyword] {}'.format(cluster_suggested_name))
#     else:
#         print('[cluster] "{}" / {:,} keywords / tags: {}'.format(cluster_suggested_name, len(cluster_members), get_frequent_words(cluster_members)))
#
#
#
# print ('\n= Performance =')
# end_time = time.time()
# print('importing: {:.3f}s'.format(start_calc_matrix - start_time))
# print('calculating distances matrix: {:.3f}s'.format(start_clustering - start_calc_matrix))
# print('clustering: {:.3f}s'.format(end_time - start_clustering))
# print('Total duration: {:.3f}s'.format(end_time - start_time))
#
#
#
#
# # dendrogram(linkage_matrix)
# # plt.show()
#
#
# '''
# Results:
# 1346 keywords
# calculating distances matrix: 514.025s
# clustering: 0.593s
# Total duration: 519.271s
#
# 20 keywords:
# calculating distances matrix: 4.866s
#
# '''