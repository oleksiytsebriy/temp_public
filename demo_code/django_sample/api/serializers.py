from .models import Category, Keyword, Attribute, Account, Configuration
from rest_framework import serializers


# class CategorySerializer(serializers.HyperlinkedModelSerializer):
class CategorySerializer(serializers.ModelSerializer):
    # children_categories = serializers.HyperlinkedRelatedField(many=True, view_name='category-detail', read_only=True)
    subcategories = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    class Meta:
        model= Category
        fields = ('id','name', 'account', 'keyword',  'description', 'created', 'updated', 'parent', 'subcategories')

# class AccountSerializer(serializers.HyperlinkedModelSerializer):
class AccountSerializer(serializers.ModelSerializer):
    # keywords= serializers.PrimaryKeyRelatedField(many=True, queryset=Keyword.objects.all()) # this returns only ids
    # keywords = serializers.HyperlinkedRelatedField(many=True, view_name='keyword-detail', read_only=True)
    keywords = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    categories = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model= Account
        fields = ('id','name','keywords','categories')


# class KeywordSerializer(serializers.HyperlinkedModelSerializer):
class KeywordSerializer(serializers.ModelSerializer):
    # attributes = serializers.HyperlinkedRelatedField(many=True, view_name='attribute-detail', read_only=True)
    attributes = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    # keyword_categories = serializers.HyperlinkedRelatedField(many=True, view_name='category-detail', read_only=True)
    keyword_categories = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model= Keyword
        fields = ('id', 'phrase', 'locode', 'account', 'attributes', 'keyword_categories', )


class AttributeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attribute
        fields = ('id','name', 'keyword', 'value', 'created', 'public')


class ConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Configuration
        fields = ('id', 'active_account', 'created')




