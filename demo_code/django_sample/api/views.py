from django.shortcuts import render
from .models import Category, Account, Keyword, Attribute
from api.models import Configuration

from api.serializers import CategorySerializer, AccountSerializer, KeywordSerializer, AttributeSerializer, ConfigurationSerializer
from rest_framework import renderers
from rest_framework import viewsets
from rest_framework.response import Response

from rest_framework.decorators import action
from django.shortcuts import get_object_or_404


from django.shortcuts import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt

import json # this is to parse Bytes type - received from Post request
import api.categories_manager as cm




class CategoryViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class KeywordViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Keyword.objects.all()
    serializer_class = KeywordSerializer

class AccountViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

class AttributeViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Attribute.objects.all()
    serializer_class = AttributeSerializer


class AccountKeywordsViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return Keyword.objects.filter(account=self.kwargs['account_pk'])

    serializer_class = KeywordSerializer


class AccountCategoriesViewSet (viewsets.ModelViewSet):
    def get_queryset(self):
        return Category.objects.filter(account=self.kwargs['account_pk'])

    serializer_class = CategorySerializer


# # This is the same as above # class AccountCategoriesViewSet but without Model ViewSets
# class AccountCategoriesViewSet (viewsets.ViewSet):
#     serializer_class = CategorySerializer
#
#     def list(self, request, account_pk=None):
#         queryset = Category.objects.filter(account=account_pk)
#         serializer = CategorySerializer(queryset, many=True)
#         return Response(serializer.data)
#
#     def retrieve(self, request, pk=None, account_pk = None):
#         queryset = Category.objects.filter(pk=pk, account=account_pk)
#         category = get_object_or_404(queryset, pk=pk)
#         serializer = CategorySerializer(category)
#         return Response(serializer.data)




class AccountKeywordsAttributesViewSet (viewsets.ModelViewSet):
    serializer_class = AttributeSerializer
    def get_queryset(self):
        return Attribute.objects.filter(keyword__account=self.kwargs['account_pk'], keyword= self.kwargs['keyword_pk'])

# class AccountKeywordsAttributesViewSet (viewsets.ViewSet): # Note regular ViewSet -  Does not contain  possibility to create update etc... - use the ModelViewSet above
#     # def get_queryset(self):
#     #     return Attribute.objects.filter(account=self.kwargs['account_pk'])
#     #
#     # serializer_class = CategorySerializer
#
#
#     serializer_class = AttributeSerializer
#
#     def list(self, request, account_pk=None, keyword_pk=None):
#         queryset = Attribute.objects.filter(keyword__account=account_pk, keyword=keyword_pk)
#         serializer = AttributeSerializer(queryset, many=True)
#         return Response(serializer.data)
#
#     def retrieve(self, request, pk=None, account_pk=None, keyword_pk=None):
#         queryset = Attribute.objects.filter(pk=pk, keyword__account=account_pk, keyword=keyword_pk)
#         keyword = get_object_or_404(queryset, pk=pk)
#         serializer = AttributeSerializer(keyword)
#         return Response(serializer.data)


class AccountCategoryKeywordsViewSet (viewsets.ModelViewSet):
    serializer_class = KeywordSerializer
    def get_queryset(self):
        return Keyword.objects.filter(keyword_categories__account=self.kwargs['account_pk'], keyword_categories=self.kwargs['category_pk'])


class AccountCategorySubcategoriesViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    def get_queryset(self):
        return Category.objects.filter(parent__account=self.kwargs['account_pk'], parent=self.kwargs['category_pk'])


class AccountRootCategoriesViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    def get_queryset(self):
        return Category.objects.filter(account=self.kwargs['account_pk'], parent__isnull=True)

@api_view(['GET'])
def change_account(request, account_pk):
    account = Account.objects.get(pk=account_pk)
    configuration = Configuration(active_account = account)
    configuration.save()
    serializer = AccountSerializer(account)
    print ('Changed Account to "{}"(id= {})'.format(serializer.data['name'], account_pk))
    print('Created new configuration record with id = {}'.format(configuration.id))

    return Response(serializer.data)


@api_view(['GET'])
def get_active_account(request):
    configuration = Configuration.objects.latest('id')
    account = configuration.active_account
    serializer = AccountSerializer(account)
    return Response(serializer.data)  # this requires @api_view




class ConfirgurationViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Configuration.objects.all()
    serializer_class = ConfigurationSerializer




@csrf_exempt # use this in order toi avoind main.js:411 POST http://127.0.0.1:8000/api/operation/ 403 (Forbidden)
# @api_view(['GET'])
def apply_operation_js(request):
    """
    Apply operation to run from JS
    """
    if request.method == 'GET':
        return HttpResponse('This is to run from JS. Call /operation to get browsable API is ')

    elif request.method == 'POST':
        data = json.loads(request.body)
        result = cm.apply_operation(data['selected_items'], data['selected_operation'],data['active_account_id'], data['selected_parameter_1'], data['selected_parameter_2'] )
        return HttpResponse(result) # Response does not work without  @api_view


@api_view(['GET', 'POST'])
def apply_operation(request):
    """
    Apply operation from browsable API
    """
    if request.method == 'GET':
        return Response('Use post method providing the data in the json format {"key" : "value"}. You have to provide selected_items, selected_operation, active_account_id, selected_feature')
    elif request.method == 'POST':
        # operation = request.data['operation']
        result = cm.apply_operation(request.data['selected_items'], request.data['selected_operation'],request.data['active_account_id'], request.data['selected_feature'])
        return Response (result)
