from django.db import models
import datetime

class Account(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-id',)


class Keyword(models.Model):
    phrase = models.CharField(max_length=100)
    locode = models.CharField(max_length=10, blank=True, default='N/A')

    account = models.ForeignKey(Account, related_name='keywords', on_delete=models.CASCADE)
    def __str__(self):
        return self.phrase

    class Meta:
        ordering = ('phrase',)

class Category(models.Model):
    name = models.CharField(max_length=60)
    account = models.ForeignKey(Account, related_name='categories', on_delete=models.CASCADE)
    keyword = models.ManyToManyField(Keyword, blank=True, null=True, related_name='keyword_categories')
    description = models.TextField(blank=True,null=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    # parent = models.CharField( max_length=60, default='root')
    parent = models.ForeignKey('Category', related_name='subcategories',blank=True,null=True,on_delete=models.CASCADE)
    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)

class Attribute(models.Model):
    # Use two attributes to record different MSV - each record contains all trends, average, date etc
    # Use two attributes to record different Location - each record contains all location entities e.g.
        # route, district (neighborhood or sublocality), city (locality), region (administrative_area_level_1) , Country, geometry (lat, lng)
    name = models.CharField(max_length=100) # e.g. MSV / Location /
    keyword = models.ForeignKey(Keyword, related_name='attributes', on_delete=models.CASCADE) # ref
    type = models.CharField(max_length=100) # e.g. value or structure not in use
    value = models.TextField(blank=True,null=True) # json corresponding to every attribute
    created = models.DateTimeField(auto_now=False, default=datetime.date.today()) # this default is used to migrate with existing records
    public = models.BooleanField(default=True) # will be used to share with other customers for the same keyword
    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)



# class AccountKeywords(models.Model): # use to upload the file with keywords
#     name =  models.CharField(max_length=100)
#     keywords_file= models.FileField()
#     def __str__(self):
#         return self.name
#     class Meta:
#         ordering = ('name',)

class Configuration(models.Model):  # let's create new obkect every time - this will let keep history
    name= models.CharField(max_length=100, null=True, default='N/A')
    active_account = models.ForeignKey(Account, related_name='configuration', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return str(self.name)
    class Meta:
        ordering = ('created',)
