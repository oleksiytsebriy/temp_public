from django.contrib import admin
from .models import Category, Attribute, Account, Keyword #,  #AccountKeywords

admin.site.register(Account)
admin.site.register(Keyword)
admin.site.register(Category)
admin.site.register(Attribute)
# admin.site.register(AccountKeywords)
