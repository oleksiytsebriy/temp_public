'''
this contains methods get location entities using google api
'''
from .models import Attribute, Keyword, Category
from .serializers import AttributeSerializer
import urllib.parse
import urllib.request
import json
import re
import nltk
import api.model_instance_manager as mim
import os.path
import pickle
import api.mst_additional as mst

def convert_utf_to_ascii(target):
    return urllib.parse.quote(target)

class Location_Categorization():
    def __init__(self, keywords, origin_category_id, location_level, ngrams_limit = 4):
        self.keywords = keywords
        self.ngrams_limit= ngrams_limit
        self.origin_category_id = origin_category_id
        self.location_entity_name = location_level
        self.location_level = self.assimilate_location_level(location_level)

        self.get_cached_location_results()

    def get_cached_location_results(self):
        root_path = os.path.dirname(__file__)
        filename= 'pickled_locations'
        self.pickled_locations_full_path = os.path.join(os.path.join(root_path, 'pickles'), filename)
        if os.path.isfile(self.pickled_locations_full_path):
            with open(self.pickled_locations_full_path, 'rb') as f:
                pickled_locations = pickle.load(f)
            self.stored_candidates = pickled_locations[0]
            self.stored_formatted_addresses = pickled_locations[1]

            self.len_stored_candidates = len(self.stored_candidates)
            self.len_stored_formatted_addresses = len(self.stored_formatted_addresses)
            print('Cached location results are found. \n'
                  'Len of stored candidates = {:,}\n'
                  'Len of stored formatted addresses = {:,}'.format(
                self.len_stored_candidates, self.len_stored_formatted_addresses))
        else:
            print ('No cached location results are found')
            self.stored_candidates= {}
            self.stored_formatted_addresses= {}
            self.len_stored_candidates = self.len_stored_formatted_addresses = 0
        self.flag_to_save_pickled_locations = False

    def assimilate_location_level(self, location_level):
        location_level_assimilation_dict= {
            'Country': 'country_long',
            'Country Short Name': 'country_short',
            'Region' : 'region_long',
            'Region Short Name': 'region_short',
            'City': 'city_long',
            'City Short Name': 'city_short',
            'District': 'district_long',
            'District Short Name': 'district_short',
            'Street' : 'route_long',
            'Postal Code': 'route_short',
            'Geo Location': 'todo'
        }

        return location_level_assimilation_dict[location_level]


    def run_categorization_by_location(self): # main function
        '''
        :param keywords: list of Keywords instances to categorize
        :param origin_category_id: id of category keywords are selected from
        :return: None
        '''

        self.get_location_entities() # this ensures checking existing/creating new location attributes for every keywords
        self.update_cached_location_results()
        self.categorize_by_location()

        return

    def update_cached_location_results(self):
        if self.flag_to_save_pickled_locations:
            pickled_locations = [self.stored_candidates, self.stored_formatted_addresses]
            with open(self.pickled_locations_full_path, "wb") as f:
                pickle.dump(pickled_locations, f, pickle.HIGHEST_PROTOCOL)
            print('Cached location results are updated. \n'
                  'Len of new stored candidates = {:,} (total = {:,})\n'
                  'Len of new stored formatted addresses = {:,} (total = {:,})'.format(
                (len(self.stored_candidates)-self.len_stored_candidates) ,
                len(self.stored_candidates),
                (len(self.stored_formatted_addresses)-self.len_stored_formatted_addresses),
                 len(self.stored_formatted_addresses)
            ))
            self.len_stored_formatted_addresses = len(self.stored_formatted_addresses)
            self.len_stored_candidates = len(self.stored_candidates)
        else:
            print('Cached location results are not updated due to no new locations obtained')

    def get_location_entities(self):
        '''
        :param keywords: list of Keyword instances
        :return: None
        '''
        print('Getting location entities...')
        total_len= len(self.keywords)
        progress = 0
        for keyword in self.keywords:
            try:
                locations_qs = Attribute.objects.filter(keyword=keyword.id, name='location')
                if len(locations_qs) == 0:
                    is_to_get_location = False
                    info_attribute_qs = Attribute.objects.filter(keyword=keyword.id,
                                                                 name='info')  # info attribute keeps the information if location was requested but not recognized
                    if len(info_attribute_qs) == 0:  # create info_attributes
                        info_attribute_id = create_info_attr(keyword.id)
                        is_to_get_location = True
                    elif not info_attribute_qs.values()[0]['value'] or (
                            'location' not in json.loads(info_attribute_qs.values()[0]['value'])):
                        is_to_get_location = True
                        info_attribute_id = info_attribute_qs.values()[0]['id']

                    if is_to_get_location:
                        locations = self.get_locations(keyword.phrase)

                        if locations:
                            for location in locations:
                                self.create_attribute_location(keyword, location)
                        else:
                            update_info_attribute(info_attribute_id, key='location', value='Google API not found')
                progress +=1
                print ('Progress: {:,} of {:,}'.format(progress, total_len))
            except Exception as e:
                print('Failure for "{}" : {}'.format(keyword,e))
        print('Getting location entities complete.')


    def create_attribute_location(self, keyword, location):
        keyword = Keyword.objects.get(id=keyword.id)
        attribute = Attribute(name='location', keyword=keyword, value=json.dumps(location))
        attribute.save()
        return

    def categorize_by_location(self):
        print ('Categorizing by location...')
        all_location_level_entities= []



        for keyword in self.keywords:
            locations_qs = Attribute.objects.filter(keyword=keyword.id, name='location')
            if len(locations_qs) > 0:
                for location in locations_qs.values():
                    location_entities_dict= json.loads(location['value'])
                    if self.location_level in location_entities_dict:
                        all_location_level_entities.append(location_entities_dict[self.location_level])
                        # print ('appending: {}'.format(location_entities_dict[self.location_level]))
        unique_location_level_entities = list(set(all_location_level_entities))
        print('Categorizing by location complete.')
        print ('All categories names:')
        print (unique_location_level_entities)
        self.create_location_categories(unique_location_level_entities)

    # def assimilate_location_entity_name(self):
    #     if 'long' in self.location_level:
    #         return self.location_level.replace('_long', ' name')
    #     if 'short' in self.location_level:
    #         return self.location_level.replace('_short', ' short name')

    def create_location_categories(self, unique_location_level_entities):
        print('Creating categories in database...')
        all_keywords_tracker = {keyword.id: None for keyword in self.keywords}

        origin_category_name = Category.objects.get(id=self.origin_category_id).name
        origin_category_account = Category.objects.get(id=self.origin_category_id).account
        #
        root_path = os.path.dirname(__file__)
        log_file_name = 'results_location_{}'.format(origin_category_account.name)
        log_file_full_path = os.path.join(os.path.join(root_path, 'results'), log_file_name)
        mst.log('Categories by {}\n'.format(self.location_level), log_file_full_path,  is_time_stamp=True)
        mst.log('All keywords:\n', log_file_full_path)
        progress = 1
        for keyword in self.keywords:
            mst.log('\t{:,}. {}'.format(progress, keyword.phrase), log_file_full_path)
            progress+=1

        new_category_name = '{} categorized by {}'.format(origin_category_name, self.location_entity_name)
        description = 'Created automatically as container for location categories'

        container_category = mim.create_category(new_category_name, origin_category_account, description=description)
        mst.log('\nAll categories: \n{}:'.format(unique_location_level_entities), log_file_full_path)
        mst.log('\nCategories details: \n', log_file_full_path)
        for location_value in unique_location_level_entities:
            mst.log('\n==========\n{}:'.format(location_value), log_file_full_path)

            keywords_for_category = get_keywords_with_attribute(self.keywords, 'location', self.location_level, location_value)

            progress = 1
            for keyword in keywords_for_category:  # mark the keyword is assigned to some category
                all_keywords_tracker[keyword.id] = True

                mst.log('\t{:,}. {}'.format(progress, keyword.phrase), log_file_full_path)
                progress += 1

            category_name = '"{}" {} category '.format(location_value, self.location_entity_name)
            description = 'Created automatically due to categorizing by location {}'.format(location_value)


            loc_category = mim.create_category(category_name, origin_category_account, parent_category=container_category, description=description)
            mim.assign_keyword_to_categories(keywords_for_category, [loc_category])

        mst.log('No location keywords:\n', log_file_full_path)
        progress = 1
        for keyword_id in all_keywords_tracker:
            if not all_keywords_tracker[keyword_id]:
                keyword = Keyword.objects.get(id=keyword_id)
                mst.log('\t{:,}. {}'.format(progress, keyword.phrase), log_file_full_path)
                progress += 1
                mim.assign_keyword_to_categories([keyword], [container_category])

    def get_locations(self,target_phrase):
        '''
        Calls to Google place API and then to google geocoding_api
        :param target_phrase: str
        :return: list of locations object
        '''
        print('Getting locations for "{}"...'.format(target_phrase))
        tokens = tokenize_sentence(target_phrase)
        tokenized_sentence_ngrams = get_ngrams_tokens(tokens)
        all_formatted_addresses = []
        for candidate in tokenized_sentence_ngrams + [target_phrase]:
            if candidate in self.stored_candidates:
                formatted_addresses = self.stored_candidates[candidate]
            else:
                formatted_addresses = get_locations_from_place_api(candidate)
                self.stored_candidates[candidate] = formatted_addresses
                self.flag_to_save_pickled_locations= True
            if formatted_addresses:
                all_formatted_addresses += formatted_addresses
        unique_formatted_address = list(set(all_formatted_addresses))

        locations = []
        for formatted_address in unique_formatted_address:
            if formatted_address in self.stored_formatted_addresses:
                address_dict = self.stored_formatted_addresses[formatted_address]
            else:
                address_dict = get_location_from_geocoding_api(formatted_address)
                self.stored_formatted_addresses[formatted_address] = address_dict
                self.flag_to_save_pickled_locations = True
            locations.append(address_dict)

        self.check_to_update_pickled_location()

        return clean_duplicates(locations)

    def check_to_update_pickled_location(self):
        if self.flag_to_save_pickled_locations and ( (len(self.stored_candidates) - self.len_stored_candidates) >100 or (len(self.stored_formatted_addresses) - self.len_stored_formatted_addresses) >50):
            self.update_cached_location_results()

def get_keywords_with_attribute(keywords, attribute_name, attribute_detail, search_value):
    keywords_for_category = {}
    for keyword in keywords:
        locations_qs = Attribute.objects.filter(keyword=keyword.id, name=attribute_name)
        for i in range(len(locations_qs)):
            value_dict = json.loads(locations_qs[i].value)
            if attribute_detail in value_dict:
                target_value = value_dict[attribute_detail]
                if target_value == search_value:
                    if keyword.id not in keywords_for_category:
                       keywords_for_category[keyword.id] = keyword

    return keywords_for_category.values()


def create_info_attr(keyword_id):
    keyword = Keyword.objects.get(id=keyword_id)
    attribute = Attribute(name='info', keyword=keyword)
    attribute.save()
    return attribute.id


def update_info_attribute(info_attribute_id, key, value):
    info_attribute = Attribute.objects.get(pk=info_attribute_id) # this could be done without serializer
    serializer = AttributeSerializer(info_attribute)
    info_attribute_serialized = serializer.data
    if info_attribute_serialized['value']:
        info_value_dict = json.loads(info_attribute_serialized['value'])
        info_value_dict[key] = value
    else:
        info_value_dict = {key : value}

    new_value = json.dumps(info_value_dict)
    setattr(info_attribute, 'value', new_value)
    info_attribute.save()

    return



def tokenize_sentence(target_str):
    target_str = re.sub(r"[_-]", ' ', target_str)
    re_pattern = r"[\w]+"
    tokens = re.findall(re_pattern, target_str)
    return [token for token in tokens if len(token) > 1]

def get_ngrams_tokens(tokens):
    '''
    :param tokens: list of str like   ['VP', 'engineering', 'for', 'adtech', 'companies', 'in', 'San', 'Francisco']
    :return: original list extended by ngrams e.g. {'San-Francisco', 'San Francisco', 'companies-in', 'adtech', 'VP-engineering',
             'in San', 'for', 'for adtech', 'companies', 'companies in', 'engineering for', 'for-adtech', 'adtech-companies', 'in-San',
            'adtech companies', 'in', 'engineering-for', 'VP', 'San', 'engineering', 'VP engineering', 'Francisco'}
    '''
    return list(set(build_ngrams_sign(tokens))) # no need to cover all others since Google API covers all those differences
                # + build_ngrams_sign(tokens,sign='-')
                # + build_ngrams_sign(tokens, sign=''))

def build_ngrams_sign(tokens, sign=' ', ngrams=4):
    return [sign.join(tokens[i:i + ngram + 1])
                for i in range(len(tokens))
                for ngram in range(ngrams)
                if i + ngram < len(tokens)
            and is_nn_pos(tokens[i:i + ngram + 1])]


def is_nn_pos(tokens):
    for token in tokens:
        pos_tag = nltk.pos_tag([token])
        if not pos_tag[0][1].startswith('NN'):  # this us for NN, NNS, NNP
            return False
    return True


def clean_duplicates(locations):
    unique_keys=[]
    resulted_unique_list = []
    for location in locations:
        key = build_key_from_values(location)
        if key not in unique_keys:
            resulted_unique_list.append(location)
            unique_keys.append(key)
    return resulted_unique_list

def build_key_from_values(location):
    key_result  = ''
    for location_entity in location:
        key_result+=str(location[location_entity ])
    return key_result

def get_locations_from_place_api(target):
    '''
    returns str  - formatted_address or name if no formatted_address  in response
    :param target:
    :return:
    '''
    # https://maps.googleapis.com/maps/api/place/findplacefromtext/json?
    #   input=Museum%20of%20Contemporary%20Art%20Australia&
    #   inputtype=textquery&
    #   fields=photos,formatted_address,name,rating,opening_hours,geometry&
    #   key=AIzaSyDTW0K1-ajiXVK1SAK_LRNbOinKjvpy_4c
    print ('\tCalling Google Place API for {}...'.format(target))
    endpoint = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?'
    api_key = 'AIzaSyDTW0K1-ajiXVK1SAK_LRNbOinKjvpy_4c'

    input = convert_utf_to_ascii(target.replace(' ', '+'))
    input_type = 'textquery'
    # fields = 'photos,formatted_address,name,rating,opening_hours,geometry'
    fields = 'formatted_address,name'
    location_request = 'input={}&inputtype={}&fields={}&key={}'.format(input, input_type, fields, api_key)

    request = endpoint + location_request
    response = urllib.request.urlopen(request).read()
    place = json.loads(response)

    if place['status']!='OK':
        return None

    candidates= place['candidates']
    location_entities= []
    for candidate in candidates:
        if 'formatted_address' in candidate and 'name' in candidate:
            location_entities.append('{}, {}'.format(candidate['name'],candidate['formatted_address']))
        elif'name' in candidate:
            location_entities.append(candidate['name'])
    return location_entities


def derive_location_value(entity_type, customized_type, address_component, address_dict):
    if entity_type in address_component['types']:
        address_dict['{}_short'.format(customized_type)]= address_component['short_name']
        address_dict['{}_long'.format(customized_type)] = address_component['long_name']
    return address_dict



def get_location_from_geocoding_api(target):
    # https://maps.googleapis.com/maps/api/geocode/json?
    # address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&
    # key=YOUR_API_KEY
    print('\t\tCalling Google Geocoding API for {}...'.format(target))
    endpoint = 'https://maps.googleapis.com/maps/api/geocode/json?'
    api_key = 'AIzaSyBl5kPp9xhQZyowt56D8X9EO1fPn6uGm8M'
    address = convert_utf_to_ascii(target.replace(' ', '+'))

    location_request = 'address={}&key={}'.format(address, api_key)
    request = endpoint + location_request

    response = urllib.request.urlopen(request).read()
    geocoding = json.loads(response)
    if geocoding ['status'] != 'OK':
        print ('Warning: geocoding does not return location recognized by place API : ')
        return None

    result = geocoding['results'][0] # assume there will be the only result for provided formatted_address
    formatted_address= result['formatted_address']
    address_components = result['address_components']
    address_dict = {'formatted_address': formatted_address}
    if 'geometry' in result and 'location' in result['geometry']:
        address_dict['lat']= result['geometry']['location']['lat']
        address_dict['lng'] = result['geometry']['location']['lng']
    for address_component in address_components:
        address_dict = derive_location_value ('route', 'route', address_component, address_dict)
        address_dict = derive_location_value('sublocality', 'district', address_component, address_dict)
        if 'district_long' not in address_dict:
            address_dict = derive_location_value('neighborhood', 'district', address_component, address_dict)
        address_dict = derive_location_value('locality', 'city', address_component, address_dict)
        if 'city_long' not in address_dict:
            address_dict = derive_location_value('postal_town', 'city', address_component,address_dict)
        address_dict = derive_location_value('administrative_area_level_1', 'region', address_component, address_dict)
        address_dict = derive_location_value('country', 'country', address_component, address_dict)
        address_dict = derive_location_value('postal_code', 'postal_code', address_component, address_dict)

    return address_dict