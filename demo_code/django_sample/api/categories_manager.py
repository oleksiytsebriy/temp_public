from .models import Account, Keyword, Attribute, Category, Configuration
import pandas as pd
import datetime
from .serializers import KeywordSerializer
import api.location_manager as lm
import api.feature_manager as fm
import api.model_instance_manager as mim

progress= 0

def upload_keywords(file_name, account_name):
    print ('Uploading keywords for account: {}'.format(account_name))
    # create Account =====
    account = mim.create_account(account_name)
    # account = Account(name=account_name)
    # account.save()
    print ('Created account with id: {}'.format(account.id))
    print('Reading file: {}'.format(file_name))
    try:
        df = pd.read_excel(file_name)
        print('Keywords amount=  {:,}'.format(len(df['Keyword']))) # this should check the 'Keywords' columns is present
    except Exception as e:
        print ('Provided file is not in the expected format #1')
        print(e)

        try: # this is to cover the format exported directly from searchlight  # possible to correct to automatically detect the keywords title
            df = pd.read_excel(file_name, skiprows=11)  # adopted for current export file from searchlight)
            print('Keywords amount=  {:,}'.format(len(df['Keyword']))) # this should check the 'Keywords' columns is present
        except Exception as e:
            print ('Provided file is not in the expected format #2')
            return 'Wrong Format'


    # create Category =====
    # category = Category(name='All keywords', parent=None, description='Created automatically due to upload', account=account)
    # category.save()
    category = mim.create_category(category_name= 'All keywords', account= account, parent_category=None, description='Created automatically due to upload')
    print ('Created category with id: {}'.format(category.id))

    cat_keywords = []
    # obsoelete working version :  df['Keyword'].apply(create_new_keyword, account = account, cat_keywords = cat_keywords)
    global progress
    progress = 0
    df['Keyword'].apply(create_keyword_in_category, account=account, categories=[category])  # , cat_keywords = cat_keywords)

    # # Update Category =====
    # category.keyword.set(cat_keywords)
    # # try later setattr(category, 'keyword', cat_keywords)
    # category.save()

    mim.create_configuration(active_account=account)
    # configuration = Configuration(active_account=account)
    # configuration.save()


def create_keyword_in_category(phrase, account, categories):
    # keyword = Keyword(phrase=phrase, account=account)
    # keyword.save()
    keyword = mim.create_keyword(phrase, account)
    mim.assign_keyword_to_categories([keyword], categories)
    mim.create_attribute (name = 'average_msv', keyword=keyword, type='msv')
    # attribute = Attribute(name='average_msv', keyword=keyword, type='msv')
    # attribute.save()
    global progress
    progress+=1
    print ('progress= {:,}'.format(progress))

    return

def apply_operation(selected_items, operation, account_id, selected_parameter_1, selected_parameter_2):
    if operation== 'Run categorization by features':
        result = run_categorization_by_features(selected_items, account_id, feature= selected_parameter_1, location_level=selected_parameter_2)
        return result
    if operation == 'Create category by features':
        result = create_category_by_features(selected_items, account_id, feature = selected_parameter_1, criteria=selected_parameter_2)
        return result
    else:
        return 'This operation is not supported.'



def run_categorization_by_features(selected_items, account_id, feature, location_level):
    if len(selected_items) != 1:
        return 'Please select one category to run categorization'
    # assume current solution allows to run catgorization for only category
    # todo some day get all selected items e.g. from selected categories
    category_id = selected_items[0]['item_id']
    keywords= Keyword.objects.filter(keyword_categories__account=account_id, keyword_categories=category_id)
    print('\nRequest for categorization by location in category with id= {} (account= {})\nLen of keywords= {:,}'.format(category_id, account_id, len(keywords)))
    # serializer = KeywordSerializer(keywords, many=True)
    # keywords_serialized = serializer.data2
    if feature=='location':
        lc = lm.Location_Categorization(list(keywords), category_id, location_level='City')
        lc.run_categorization_by_location()

    return 'Categorization complete'


def create_category_by_features(selected_items, account_id, feature, criteria):
    if len(selected_items) != 1:
        return 'Please select one category to run categorization'
    # assume current solution allows to run catgorization for only category
    # todo some day get all selected items e.g. from selected categories
    category_id = selected_items[0]['item_id']
    keywords= Keyword.objects.filter(keyword_categories__account=account_id, keyword_categories=category_id)
    print('\nRequest for creating category by features for keywords from category with id= {} (account= {})\nLen of keywords= {:,}'.format(category_id, account_id, len(keywords)))

    # if feature=='Brand' and criteria == 'Is n':
    result = fm.create_category_by_features(list(keywords), category_id, feature, criteria)
    return result


