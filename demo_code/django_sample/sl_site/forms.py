from django import forms
from .models import Document
from api import serializers
# import api.categories_manager as cm



class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ('name', 'document')

    # def clean_document(self): # use it to access the provided data in the form
    #     name = self.cleaned_data.get('name')
    #     document = self.cleaned_data.get('document')
    #     cm.upload_keywords (document, account_name=name)
    #     return # think what to return
