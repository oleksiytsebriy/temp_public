from django.http import HttpResponse
from django.shortcuts import render

from .forms import DocumentForm
import api.categories_manager as cm
from django.shortcuts import redirect

def index(request):
    # return HttpResponse("<h2>hello views.py</h2>")
    content= {
        'category_path': 'Root',
        'category_name':'All Keywords',
    }
    return render(request, 'sl_site/manage_categories.html', content)





# def model_form_upload(request):
#     if request.method == 'POST':
#         form = DocumentForm(request.POST, request.FILES)
#         if form.is_valid():
#             name = form.cleaned_data.get('name')
#             document = form.cleaned_data.get('document')
#             cm.upload_keywords (document, account_name=name)
#                 # return redirect('home')
#     else:
#         form = DocumentForm()
#     return render(request, 'sl_site/account_form.html', {
#         'form': form
#     })


def manage_accounts(request):
    # return HttpResponse("<h2>hello views.py</h2>")
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES) # don't undestand this the parameters actually
        if form.is_valid():
            name = form.cleaned_data.get('name')
            document = form.cleaned_data.get('document')
            cm.upload_keywords (document, account_name=name)
            return redirect('/sl/manage_accounts/')
    else:
        form = DocumentForm()

    content= {
        'form': form
    }
    return render(request, 'sl_site/manage_accounts.html', content)

def handle_parameters(request, *args, **kwargs):  # educational - learning capturing parameters
    # # print ('request.GET', request.GET["origin"])
    origin = request.GET.get('origin', None)
    destination = request.GET.get('destination', None)
    print('origin= {}, destination= {}'.format(origin,destination))
    steps= ['step 1', 'step 2', 'step 3', 'step 4', 'step 5']
    content = {
        'origin': origin,
        'destination': destination,
        'steps': steps
    }
    if not origin or not destination:
        content= None

    # return HttpResponse('<p>{}</p><p>{}</p>'.format(origin,destination))
    return render(request, 'sl_site/display_direction.html', content)