from django.db import models
class Document(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255, blank=True) # remove
    document = models.FileField(upload_to='documents/')
    uploaded_at = models.DateTimeField(auto_now_add=True)
