

function which_nav_selected() {    
    return document.querySelector(".nav_list li.selected").id;
}


function handle_response_all_accounts (response) {
    var actual_JSON = JSON.parse(response);
    console.log("All accounts response:");
    console.log(actual_JSON);
    // all_accounts_list= document.getElementById("all_accounts_list");

    for (var i=0; i<actual_JSON.length;i++) { // account in all_accounts    
        item_name= actual_JSON[i]['name']
        account_id= actual_JSON[i]['id']
        account_keywords_length = actual_JSON[i]['keywords'].length
        account_categories_length = actual_JSON[i]['categories'].length
        

        // account_categories_length= actual_JSON[i]['caregories'].length

        // $('#all_accounts_list').append(
        //     $('<li>').attr("class", "my_panel_item d-flex ").append(
        //         $('<button>').attr('class','btn btn-link').attr('account_id', ("acc_"+ account_id)).append(account_name), 
        //         $('<span>').attr("class","badge badge-secondary badge-pill").append(3), 
        //         $('<span>').attr("class","badge my_panel_green badge-pill").append(4), 
        //         ));



        $('#all_accounts_table').append(
            $('<tr>').attr("class", "my_panel_item").attr('data-account_id', account_id).append(
                // $('<td>').attr('class',"my_panel_item_name").append(), 
                $('<td>').append(
                    $('<div>').attr('class',"my_panel_item_name").append(item_name)), 


                $('<td>').append(account_categories_length), 
                $('<td>').append(account_keywords_length) 
                ));



    }
    $(".my_panel_item").on("click", function(){
        var account_id = $(this).attr ("data-account_id");
        console.log('Request to change active account to: '+ account_id);
        url = "/api/change_account/"+account_id; 
        console.log('Url: '+ url);        
        loadJSON(url, handle_response_change_account);
    })
    $(".my_panel_item").on("mouseenter", function(){
        $(this).css({background : '#fff'});
    })
    $(".my_panel_item").on("mouseleave", function(){
        $(this).css({background : 'initial'});
    })



}


function handle_response_change_account(response){
    var actual_JSON = JSON.parse(response);
    document.getElementById("current_account").innerHTML = actual_JSON['name'];
    alert('Account changed to : '+  actual_JSON['name']);

}





// function handle_response_active_account(response) {
//     var actual_JSON = JSON.parse(response);
//     console.log('Active account:');
//     console.log(actual_JSON);
//     active_account_name = actual_JSON['name']

// }


function populate_active_account(){
    loadJSON("/api/active_account/", handle_response_active_account);
}




function populate_category_content(category_id){
    if (category_id==0){
        url =  "/api/accounts/"+active_account_id+"/rootcategories/";
        loadJSON(url, handle_response_subcategories, category_id);
    }
    else{        
        url =  "/api/accounts/"+active_account_id+"/categories/"+category_id+"/subcategories";
        console.log("Requesting url: "+ url);
        loadJSON(url, handle_response_subcategories, category_id);
    }


}


function build_subcategories(actual_JSON, category_id){
    // current_el= $('[data-item_id="'+category_id+'"]')
    current_el= $('[data-item_id="'+category_id+'"][data-type="category"]')  
    current_indent = Number(current_el.attr('data-indent_level')); 
    console.log ('current_indent: ', current_indent)
    target_indent = current_indent +1;
    indent_value = current_indent*20; 
    //console.log("current_indent= "+current_indent);

    target_el = $('<div>').attr('data-expanded_category_id', category_id);
    target_el.insertAfter(current_el);
    
    for (var i=0; i<actual_JSON.length;i++) { 
        item_name= actual_JSON[i]['name'];
        item_id= actual_JSON[i]['id'];
        category_keywords_length = actual_JSON[i]['keyword'].length;
        subcategories_length = actual_JSON[i]['subcategories'].length;

        // build row of subcategories/root categories ==================
        target_el.append(
            $('<div>').attr("class", "row my_panel_row").attr('data-item_id',item_id).attr('data-type',"category").attr('data-indent_level',target_indent).attr('data-status','initial').append(
                $('<div>').attr("style", "width:"+(20+indent_value)+ "px;").attr('class','my_panel_item').append(
                   $('<input>').attr("type", "checkbox")),
                $('<div>').attr("style", "width:20px;").attr('class','item_extend').append(
                   $('<img>').attr("src", "/static/img/expand_icon_grey.png").attr('class','icon')),
                $('<div>').attr("style", "width:20px;").attr('class','my_panel_item').append(
                    $('<img>').attr("src", "/static/img/folder_icon.png").attr('class','icon')),
                $('<div>').attr("style", "width:"+(460-indent_value)+ "px;").attr('class','my_panel_item my_panel_item_name').append(item_name), //+' (id = '+item_id+')'),
                $('<div>').attr("style", "width:40px;").attr('class','my_panel_item').append(subcategories_length),
                $('<div>').attr("style", "width:40px;").attr('class','my_panel_item').append(category_keywords_length)
            )
        );
    }

    // apply expand/collapse==================
    target_el.find(".item_extend").on("click", function(){        
        category_id = $(this).parent().attr("data-item_id");
        status = $(this).parent().attr("data-status");        
        if (status=='initial'){
            $(this).parent().attr('data-status', 'expanded');
            $(this).html($('<img>').attr("src", "/static/img/collaps_icon_grey.png").attr('class','icon'));
            console.log("Expand category: " +category_id);                 
            populate_category_content(category_id);
        }
        else {
            if (status=='collapsed'){
            $(this).parent().attr('data-status', 'expanded');
            $(this).html($('<img>').attr("src", "/static/img/collaps_icon_grey.png").attr('class','icon'));
            $('[data-expanded_category_id ='+category_id+']').show();            
            
            }
            else{
                $(this).parent().attr('data-status', 'collapsed');
                $(this).html($('<img>').attr("src", "/static/img/expand_icon_grey.png").attr('class','icon'));
                $('[data-expanded_category_id ='+category_id+']').hide();

            }
        }  
    })

    $(target_el).find(".my_panel_row .my_panel_item").on("click", function(){        
        if ($(this).parent().hasClass("my_panel_row_selected")){
            $(this).parent().removeClass('my_panel_row_selected');
            $(this).parent().find('input').prop( "checked", false );
        }
        else{
            $(this).parent().addClass('my_panel_row_selected');
            $(this).parent().find('input').prop( "checked", true );
        }
    })


    // $(".my_panel_row").on("mouseenter", function(){
    //     $(this).css({background : '#fff'});
    // })
    // $(".my_panel_row").on("mouseleave", function(){
    //     $(this).css({background : 'initial'});
    // })

}






function handle_response_subcategories(response, args){ // this is called when requested expand category or popuate  root 
    // console.log("args:");
    // console.log(args);
    category_id = args[0];    

    var actual_JSON = JSON.parse(response);
    console.log("Building Subcategories for category id = "+ category_id+":" );
    console.log(actual_JSON);
    
    if (actual_JSON.length > 0){
       build_subcategories(actual_JSON, category_id);
    }
    populate_category_keywords(category_id);

}


function populate_category_keywords(category_id){
    url =  "/api/accounts/"+active_account_id+"/categories/"+category_id+"/keywords";
    console.log("Requesting url: "+ url);
    loadJSON(url, handle_response_keywords, category_id);
}


function handle_response_keywords(response, args){ 
    category_id = args[0];    

    var actual_JSON = JSON.parse(response);
    console.log("Building keywords for category id = "+ category_id+":" );
    console.log(actual_JSON);
    
    if (actual_JSON.length > 0){
       build_keywords(actual_JSON, category_id);
    }
}


function build_keywords(actual_JSON, category_id){
    // current_el= $('[data-item_id="'+category_id+'"]')
    current_el= $('[data-item_id="'+category_id+'"][data-type="category"]')
    current_indent = Number(current_el.attr('data-indent_level')); 
    target_indent = current_indent +1;
    indent_value = current_indent*20; 
    console.log ('current_indent= ', current_indent)
    target_el  = $('[data-expanded_category_id="'+category_id+'"]')
    if (target_el.length == 0){
        target_el = $('<div>').attr('data-expanded_category_id', category_id);
        target_el.insertAfter(current_el);
    }
    for (var i=0; i<actual_JSON.length;i++) { 
        item_name= actual_JSON[i]['phrase'];
        item_id= actual_JSON[i]['id'];

        // build row of keyword ==================
        target_el.append(
            $('<div>').attr("class", "row my_panel_row").attr('data-item_id',item_id).attr('data-type',"keyword"). append(
                $('<div>').attr("style", "width:"+(20+indent_value)+ "px;").attr('class','my_panel_item').append(
                   $('<input>').attr("type", "checkbox")),
                $('<div>').attr("style", "width:20px;").attr('class','my_panel_item'),
                $('<div>').attr("style", "width:20px;").attr('class','my_panel_item').append(
                    $('<img>').attr("src", "/static/img/file_icon.png").attr('class','icon')),
                $('<div>').attr("style", "width:"+(460-indent_value)+ "px;").attr('class','my_panel_item my_panel_item_name').append(item_name),
                $('<div>').attr("style", "width:40px;").attr('class','my_panel_item'),
                $('<div>').attr("style", "width:40px;").attr('class','my_panel_item'),
            )
        );
    }
    $(target_el).find(".my_panel_row .my_panel_item").on("click", function(){        
        if ($(this).parent().hasClass("my_panel_row_selected")){
            $(this).parent().removeClass('my_panel_row_selected');
            $(this).parent().find('input').prop( "checked", false );


        }
        else{
            $(this).parent().addClass('my_panel_row_selected');
            $(this).parent().find('input').prop( "checked", true );
        }
    })


    
    
    // $(".my_panel_row").on("mouseenter", function(){
    //     $(this).css({background : '#fff'});
    // })
    // $(".my_panel_row").on("mouseleave", function(){
    //     $(this).css({background : 'initial'});
    // })

}


function handle_response_active_account(response) {
    var actual_JSON = JSON.parse(response);
    console.log('Active account:');
    console.log(actual_JSON);

    document.getElementById("current_account").innerHTML = actual_JSON['name'];
    active_account_id = actual_JSON['id'];

    if (which_nav_selected() == 'nav_categories'){ 
        populate_category_content(0);
    }

}




function set_form_styles(){    
    document.querySelector("#upload_form button").className= "btn-sm my_padding my_panel_green";
    document.querySelectorAll("#upload_form p label")[1].innerHTML= ""; // remove document label 

}

function populate_all_accounts(){
    loadJSON("/api/accounts/", handle_response_all_accounts);
}


function add_event_listeners_account_nav(){
    $("#add_account_btn").on("click", function(){
       $("#add_account_form").show(); 
       $("#add_account_btn").hide();
    });

}

// function run_categorization_by_features_click(selected_items_el){
//     if (selected_items_el.length>1){
//         alert("Cannot run categorization for multi-selected items");
//         return
//     }
//     if (selected_items_el.length==0){
//         alert("Please select category to run the categorization in");
//         return
//     }

//     data_type = $('.my_panel_row_selected').attr('data-type');
//     if (data_type=="category"){
//         category_id= $('.my_panel_row_selected').attr('data-item_id');
//         run_categorization_by_features(category_id);
//     }
//     else{
//         alert("Please select category to run the categorization in");    
//     }
    
// }


function run_categorization_by_features_click(){
    console.log("Selected operation to run categorization by features");
    // selected_operation = 'run_categorization_by_features';
    // $('#features_options').show(); 

}

// function feature_criteria_click(criteria_text){
//     console.log("Selected feature criteria " + criteria_text);
//     selected_parameter_2 = criteria_text
//     // selected_operation = 'run_categorization_by_features';
//     // $('#features_options').show();

// }




function run_creating_category_by_features_click(){
    console.log("Selected operation to create category by features");
    // selected_operation = 'run_creating_category_by_features';
    // $('#features_options').show();
    // $('#feature_criteria').show();
}


function feature_location_click(){
    console.log('Selected fauture: location');
    selected_parameter_1 = 'location'; 
    // $('#location_level').show();
    // $('#apply_operation_button').show();

}


// function location_level_click(location_level_text){
//     selected_parameter_2 = location_level_text; 
//     // $('#apply_operation_button').show();    
// }


function add_listener_to_change_the_dropdon_text(dropdown_id){
    $('#'+dropdown_id).find('.dropdown-item').on("click", function(){
         $('#'+dropdown_id).find(".btn:first-child").text($(this).text());
         $('#'+dropdown_id).find(".btn:first-child").val($(this).text());
    })

}

function add_listeners_to_dropdowns(){
    // add common functionality
    var dropdown_controls_list= ['operations_options', 'features_options','location_level', 'feature_criteria'];
    for (var i=0; i<dropdown_controls_list.length; i++){
        add_listener_to_change_the_dropdon_text(dropdown_controls_list[i]);
    }
    // add specific functionality 
    add_listeners_to_operations_options();
    add_listeners_to_features_options();
    add_listeners_to_location_level();
    add_listeners_to_features_criteria();


}

function add_listeners_to_operations_options(){
    $("#operations_options .dropdown-item").on("click", function(){        
        // selected_items_el = $('.my_panel_row_selected')
        // option_id=$(this).attr("id");
        // option_text= $(this).text();        
        selected_operation = $(this).text();
        if (selected_operation=="Run categorization by features"){
            console.log('Selected to run categorization by features');
            show_only_related_controls(['operations_options', 'features_options']) ;
            // ['operations_options', 'features_options', 'location_level', 'feature_criteria', 'apply_operation_button']
            run_categorization_by_features_click();            
            
        }
        else{
            if (selected_operation=="Create category by features"){
                show_only_related_controls(['operations_options', 'features_options', 'feature_criteria']) ;
                run_creating_category_by_features_click(); 
            
            }
        }

    });
}

function add_listeners_to_features_options(){
    $("#features_options .dropdown-item").on("click", function(){        
        // option_text= $(this).text();            
        selected_parameter_1 = $(this).text();     
        if (selected_parameter_1=="Location"){
            console.log('Selected Location');            
            if (selected_operation== 'Create category by features'){
                show_only_related_controls(['operations_options', 'features_options', 'location_level','feature_criteria']) ;
            }
            if (selected_operation== 'Run categorization by features'){
                show_only_related_controls(['operations_options', 'features_options', 'location_level']) ;
            }
            // ['operations_options', 'features_options', 'location_level', 'feature_criteria', 'apply_operation_button']
            feature_location_click();
        }
        else{            
            if (selected_operation== 'Create category by features'){
                show_only_related_controls(['operations_options', 'features_options','feature_criteria']) ;
            }            
            if (selected_operation== 'Run categorization by features'){
                show_only_related_controls(['operations_options', 'features_options', 'apply_operation_button']) ;
            }
            // simple_feature_click ();      
        }        

    });
}

// function simple_feature_click(){
//     console.log('TODO');
// }

function add_listeners_to_location_level(){
    $("#location_level .dropdown-item").on("click", function(){        
        option_text= $(this).text();    
        show_only_related_controls(['operations_options', 'features_options', 'location_level','apply_operation_button']) ;
        selected_parameter_2 = location_level_text; 
        // location_level_click(option_text);
    });
}

function add_listeners_to_features_criteria(){
    $("#feature_criteria .dropdown-item").on("click", function(){
        show_only_related_controls(['operations_options', 'features_options', 'feature_criteria', 'apply_operation_button']) ;
        // selected_parameter_2= $(this).text();
        

        // feature_criteria_click(option_text);

    });
}




// function hide_all_location_controls(){
//     $('#location_level').hide();
//     $('#apply_operation_button').hide();

// }

function show_only_related_controls(controls_to_show){   
    console.log('Show controls: '+ controls_to_show);     
    for (var j=0; j< all_controls_to_show.length;j++ ){ // hide all 
        $('#'+all_controls_to_show[j]).hide()
    }
    for (var i=0; i< all_controls_to_show.length;i++ ){ // show 
        for (var j=0; j< controls_to_show.length;j++ ){
            if (controls_to_show[j] == all_controls_to_show[i]){
                $('#'+controls_to_show[j]).show()
                // $('#'+controls_to_show[j]+' button').prop("selectedIndex", 0);
            }
        }
    }
}

// function hide_all_feature_controls(){
//     $('#features_options').hide();
//     $('#location_level').hide();
//     $('#apply_operation_button').hide();   

// }

function apply_operation(){
    selected_elements= $('.my_panel_row_selected');
    selected_items= []
    for (var i =0;i< selected_elements.length;i++){
        item_type=  selected_elements[i].getAttribute('data-type');;
        item_id= selected_elements[i].getAttribute('data-item_id');
        selected_items[i] = {'item_type':item_type, 'item_id':item_id};
    }

    // data = {'selected_items':selected_items , 'selected_operation': selected_operation , 'active_account_id':active_account_id, 'selected_feature':selected_feature};
    data = {'selected_items':selected_items , 'selected_operation': selected_operation , 'active_account_id':active_account_id, 'selected_parameter_1':selected_parameter_1, 'selected_parameter_2':selected_parameter_2};
    // console.log('selected items:');
    // console.log(data['selected_items']);

    send_post_request("/api/operation_js/", data);

}


function add_listener_to_buttons(){
    $("#apply_operation_button").on("click", function(){
        apply_operation()
    })
}


function loadJSON(url, callback, ...args) {
    var request = new XMLHttpRequest();
    request.overrideMimeType("application/json");    
    request.open('GET', url, true);  //true means assynchronous requests 
    request.onreadystatechange = function () {
          if (request.readyState == 4 && request.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(request.responseText, args);
          }
    };
    request.send(null);
}



function send_post_request(url, data){
    var request = new XMLHttpRequest();
    request.overrideMimeType("application/json");        
    request.open('POST', url, true);

    //Send the proper header information along with the request
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    request.onreadystatechange = function() {//Call a function when the state changes.
        if(request.readyState == 4 && request.status == 200) {                        
            // alert(request.responseText);
            alert (request.responseText);
            window.location.reload();

        }
    }
    // data = {"operation":"test"}
    request.send(JSON.stringify(data))
}



function add_event_listeners_nav(){
    document.getElementById("nav_accounts").addEventListener("click", function (){
        window.location.href = '/sl/manage_accounts';
    })

    document.getElementById("nav_categories").addEventListener("click", function (){
       // document.getElementById("categorize_keywords").className="selected";
        window.location.href = '/sl';

    })

}

function init_global_variables(){
    window.selected_operation= '';
    window.selected_parameter_1 = '';
    window.selected_parameter_2 = [];
    window.all_controls_to_show = ['operations_options', 'features_options', 'location_level', 'feature_criteria', 'apply_operation_button'];
    // window.selected_parameter_3 = [];
    window.active_account_id = '';
}

function download_nav_accounts(){
    console.log("nav_accounts download");
    populate_active_account();
    set_form_styles();
    populate_all_accounts();
    add_event_listeners_account_nav();

}

function download_nav_categories(){
    console.log("nav_categories download");
    init_global_variables()
    add_listeners_to_dropdowns();
    add_listener_to_buttons();
    populate_active_account();

    //todo     
}


function execute_download(){
    if (which_nav_selected() == 'nav_accounts'){ // download  nav_accounts
        download_nav_accounts();
        return 
    }
    if (which_nav_selected() == 'nav_categories'){ // download  nav_categories
        download_nav_categories();
        return
    }


}

// ========================== execution ============================================
$(document).ready(function(){ // make sure the all stuff is loaded - recommended 
    add_event_listeners_nav();
    execute_download();    
    // alert("All scripts completed.");
})