from django.apps import AppConfig


class SlSiteConfig(AppConfig):
    name = 'sl_site'
