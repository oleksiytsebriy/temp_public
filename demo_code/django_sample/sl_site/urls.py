from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    # url(r'^form', views.model_form_upload, name='model_form_upload'),

    url(r'^manage_accounts', views.manage_accounts, name='manage-account'),
    url(r'^educational', views.handle_parameters),
]

