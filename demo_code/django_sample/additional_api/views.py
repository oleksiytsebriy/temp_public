from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from api.models import Category, Account, Keyword, Attribute
from .serializers import KeywordSerializer, CategorySerializer


class AllKeywords_CategoryList(APIView):
    """
    List all keywords - assume to execute for "all keywords" category
    """
    def get(self, request, format=None):
        keywords= Keyword.objects.all()
        serializer = KeywordSerializer(keywords, many=True, context={'request': request})
        return Response(serializer.data)


class CategoryKeywordsList(APIView):
    """
    List all keywords assigned to certain category
    """
    def get(self, request, pk, format=None):
        keywords= Keyword.objects.filter(keyword_categories__in=[pk])
        serializer = KeywordSerializer(keywords, many=True, context={'request': request})
        return Response(serializer.data)

class RootCategoriesList(APIView):
    """
    List Categories  without parent category
    """
    def get(self, request):
        root_categories= Category.objects.filter(parent_lookup__isnull=True)
        serializer = CategorySerializer(root_categories, many=True) #, context={'request': request})
        return Response(serializer.data)


class SubcategoriesList(APIView):
    """
    List all subcategories of certain category
    """
    def get(self, request, pk):
        subcategories = Category.objects.filter(parent_lookup__exact=pk)
        serializer = CategorySerializer(subcategories, many=True) # , context={'request': request})
        return Response(serializer.data)

