from api.models import Category, Keyword, Attribute, Account
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):
    children_categories = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    class Meta:
        model= Category
        fields = ('name', 'account', 'keyword',  'id', 'description', 'created', 'updated', 'parent_lookup','children_categories')


# class AccountSerializer(serializers.HyperlinkedModelSerializer):
#     # keywords= serializers.PrimaryKeyRelatedField(many=True, queryset=Keyword.objects.all()) # this returns only ids
#     keywords = serializers.HyperlinkedRelatedField(many=True, view_name='keyword-detail', read_only=True)
#     categories = serializers.HyperlinkedRelatedField(many=True, view_name='category-detail', read_only=True)
#
#     class Meta:
#         model= Account
#         fields = ('name','keywords','categories')

class KeywordSerializer(serializers.ModelSerializer):
    attributes = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    # keyword_categories = serializers.ManyRelatedField(many=True, read_only=True)

    class Meta:
        model= Keyword
        fields = ('phrase', 'locode', 'account', 'id', 'attributes', 'keyword_categories', )


# class AttributeSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Attribute
#         fields = ('name', 'keyword', 'type', 'value')
