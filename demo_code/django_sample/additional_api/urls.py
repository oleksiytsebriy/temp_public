from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^cat_keywords/$', views.AllKeywords_CategoryList.as_view()),
    url(r'^cat_keywords/(?P<pk>[0-9]+)/$', views.CategoryKeywordsList.as_view()),
    url(r'^subcategories/$', views.RootCategoriesList.as_view()),
    url(r'^subcategories/(?P<pk>[0-9]+)/$', views.SubcategoriesList.as_view()),

]

