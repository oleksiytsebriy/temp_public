'''
This is file used in docker container
Contains functions to access AWS
This file is mostly used by insight_stream development
Contains some redundant methods being copied from original one that is used not from docker

Requires .env file with credentials to AWS

'''

import os
import subprocess
import boto3
import warnings

# set the environment variables:
AWS_VAR_NAMES = ['AWS_ACCESS_KEY_ID', 'AWS_SECRET_ACCESS_KEY']

AWS_access_pair= {}
AWS_access_pair['AWS_ACCESS_KEY_ID'] = os.environ['AWS_ACCESS_KEY_ID']
AWS_access_pair['AWS_SECRET_ACCESS_KEY'] = os.environ['AWS_SECRET_ACCESS_KEY']  # cfg.AWS_access_pair['AWS_SECRET_ACCESS_KEY']





def check_aws_keys(verbose=False):
    ''' Notify user if AWS keys cannot be found in environment '''
    for name in AWS_VAR_NAMES:
        var = AWS_access_pair[name]
        if var is None:
            raise EnvironmentError('Could not find {} in environment variables'.format(var))
        elif verbose:
            print( 'Found {} in environment variables'.format(var) )

def validate_s3_path(path):
    '''
    Allows to avoid some errors due to necessary prefix of  s3 url

    :param path:
    :return: string with corrected path
    Examples:
        print (validate_s3_path('s3://cond-science/avashchenko/MSV/Prodex'))
        print (validate_s3_path('s3:/cond-science/avashchenko/MSV/Prodex'))
        print(validate_s3_path('/cond-science/avashchenko/MSV/Prodex'))
        print(validate_s3_path('cond-science/avashchenko/MSV/Prodex'))

        all returns s3://cond-science/avashchenko/MSV/Prodex

    '''
    init_path= path
    if path.startswith('otsebriy') or path.startswith('skennerly') or path.startswith('avashchenko'):
        path = '{}{}'.format('s3://cond-science/', path)
    elif path.startswith('s3://'):
        pass
    elif path.startswith('//'):
        path = '{}{}'.format('s3:',path)
    elif path.startswith('/'):
        if path.startswith('/otsebriy') or path.startswith('/skennerly') or path.startswith('/avashchenko'):
            path = '{}{}'.format('s3://cond-science', path)
        else:
            path = '{}{}'.format('s3:/', path)
    elif path.startswith('s3:/'):
        path = path.replace('s3:/', 's3://')
    else:
        path = '{}{}'.format('s3://', path)
    if path != init_path:
        print ('Path to s3 corrected \n\tProvided: {}\n\tNew: {}'.format(init_path, path))
    return path


def push_folder(local_path, target_path, verbose=False):
    '''
    Copy content of local folder to the science bucket.
    Example:
        import AWS_manager as aws
        local_path = '/Users/new/science_1/data/otsebriy/Test_folder'
        target_path = 's3://cond-science/otsebriy/Test_bucket_folder'
        aws.push_folder(local_path, target_path, verbose=True)
    Inputs:
        local_path  - string: full path, as it is seen by command pwd in local directory
        target_path - string: full path, as it accesses in s3
        verbose [optional] - bool: send all AWS CLI outputs to screen
    Warning 1: Does not delete files, but may overwrite existing files in the science bucket!
    Warning 2: It allows to push to only folder of owner
    Note 1 : if s3 folder does not exist, it will create new one
    Note 2 : local or target path could contain '/' at the end. It works either with or without it

    '''



    check_aws_keys() # breaks the execution if keys pair is not provided
    target_path = validate_s3_path(target_path)

    print("Pushing local folder: {} \n \tto destination: {}".format(local_path, target_path))
    cmd = 'aws s3 sync'.split()
    cmd += [local_path, target_path]

    # Make sure to provide '/' at the end of target path for folder
    if target_path[-1]!= '/':
        print ('target_path corrected. Provided: {}'.format(target_path))
        target_path = target_path + '/'
        print('Corrected to: {}'.format(target_path))


    if verbose:
        bash_cmd= ' '.join(cmd)
        print('Running the command: {}'.format(bash_cmd))
    try:
        output= subprocess.check_output(cmd, universal_newlines=True) # Tell awscli to run command
        if verbose and output:
            print(output)
        print('Pushing folder completed')

    except Exception as message:
        if verbose:
            print('Error occurred at calling AWS. Details: \n{}\nPushing folder failed!'.format(
                message))
        else:
            print('Error occurred at calling AWS. Call function with verbose = True to get more details')

def upload_folder(local_path, is_to_overwrite, verbose=0):
    '''
    Similar to push_folder but allows to upload just files that are not in s3 yet and uploads to target folder with te same relative path
    As the result it saves the time for uploading the uploaded files already and also allows to avoid risk to overwrite the necessary files in s3
    Copy content of local folder to the science bucket.

    Warning 1: if is_to_overwrite = True, it overwrites the existing files in the science bucket!
    Warning 2: It allows to push to only folder of owner
    Note 1 : if s3 folder does not exist, it will create new one
    Note 2 : local or target path could contain '/' at the end. It works either with or without it
    '''
    target_path = mst.get_s3_path(local_path)

    prefix_and_key = mst.get_s3_prefix_and_key (local_path)
    # print ('prefix_and_key= {}'.format(prefix_and_key))

    if is_to_overwrite or not is_folder_in_s3(prefix_and_key=prefix_and_key,verbose= verbose):  # upload whole folder
        push_folder(local_path, target_path, verbose=verbose)
        return

    print ('Target folder {} is found in s3. Updating its content...'.format(target_path))
    check_aws_keys() # breaks the execution if keys pair is not provided
    target_path = validate_s3_path(target_path)

    print("Uploading files from local folder: {} \n \tto destination: {}...".format(local_path, target_path))

    all_files_list = mst.get_all_files(local_path)
    for file_name in all_files_list:
        prefix_and_key = mst.get_s3_prefix_and_key(file_name)
        file_target_path = mst.get_s3_path(file_name)
        if not is_file_in_s3(prefix_and_key=prefix_and_key, verbose= verbose):
            print ('Uploading {}...'.format(file_name))

            push_file(file_name,file_target_path)
        else:
            print('File is found in s3: {}. \nUploading skipped.'.format(file_target_path))


def push_file(full_file_name, target_path, date_mark = None): # upload to s3
    '''
    Copy local file to the science bucket
    Example:
        import AWS_manager as aws
        full_file_name = '/Users/new/science_1/data/otsebriy/Test_folder/all_duplicates_canonical.csv'
        target_path = 's3://cond-science/otsebriy/Test_bucket_folder/'
        aws.push_file(full_file_name, target_path, verbose=True)
    Inputs:
        full_file_name  - string: full path inculding file name (path as it is seen by command pwd in local directory)
        target_path - string: full path to folder, as it accesses in s3
        verbose [optional] - bool: send all AWS CLI outputs to screen
    Warning 1: Does not delete files, but may overwrite existing files in the science bucket!
    Warning 2: It allows to push to only to folders of owner
    Note 1: if s3 folder does not exist, it will create new one
    Note 2 : in case of using Cyberduck, push refresh button having open folder
    '''

    check_aws_keys()
    print ('target_path= ', target_path)
    target_path = validate_s3_path(target_path)
    print ('target_path= ', target_path)

    print("Pushing local file: {} \n \tto destination: {}".format(full_file_name, target_path))

    # Build command as a list of strings
    cmd = 'aws s3 cp'.split() # Note : sync for folders

    cmd += [full_file_name, target_path]

    bash_cmd= ' '.join(cmd) # create cmd to print
    print('Running the command: {}\n(This may take some minutes in case of large file)'.format(bash_cmd))

    try:
        output = subprocess.check_output(cmd, universal_newlines=True)  # Tell awscli to run command
        if output:
            print(output)

        print('Pushing file completed')

    except Exception as message:
        print('Error occurred at calling AWS. Details: \n{}\nPushing file failed!'.format(
            message))


def pull_folder(source_path, local_path=None, verbose=0):
    '''
    Copy content of s3 folder to the local folder.
    Example:
        import AWS_manager as aws
        source_path = 's3://cond-science/otsebriy/Test_bucket_folder' # make sure the folder exists. Otherwise nothing will get copied and no error occurs
        local_path = '/Users/new/science/data/otsebriy/Test_folder3'
        aws.pull_folder(source_path,local_path, verbose=True)
    Inputs:
        source_path - string: full path, as it accesses in s3
        local_path - string: full path, as it is seen by command pwd in local directory
        verbose [optional] - bool: send all AWS CLI outputs to screen
    Warning 1: Does not delete files, but may overwrite existing files
    Warning 2: If source folder does not exist then nothing gets copied and no error occurred!!!
    Note 1 : if local folder does not exist, it will create new one
    Note 2 : local or target path could contain '/' at the end. It works either with or without it
    Note 3 : It is configured to allow to pull any content from science bucket

    '''

    check_aws_keys() # breaks the execution if keys pair is not provided
    source_path = validate_s3_path(source_path)


    if not local_path:
        local_path =  mst.get_local_path(source_path)
        print ('Local path resolved to: {}'.format(local_path))


    if verbose>0:
        print("Pulling s3 folder: {} \n \tto destination: {}\n(This may take some minutes in case of large files)".format(source_path, local_path))

    cmd = 'aws s3 sync'.split()
    cmd += [source_path, local_path]

    if verbose>1:
        bash_cmd= ' '.join(cmd)
        print('Running the command: {}'.format(bash_cmd))
    try:
        output = subprocess.check_output(cmd, universal_newlines=True) # Tell awscli to run command
        if verbose>1:
            if output:
                print(output)
            print('Pulling folder completed')

    except Exception as message:
        if verbose>1:
            print('Error occurred at calling AWS. Details: \n{}\nPulling folder failed!'.format(
                message))
        else:
            print('Error occurred at calling AWS. Call function with verbose = True to get more details')


def pull_file(source_path, local_path, verbose=0, local_file_name = None):
    '''
    Copy file fro s3 to the local folder
    Example:
        import AWS_manager as aws
        source_path = 's3://cond-science/otsebriy/Test_bucket_folder/all_duplicates_canonical.csv'
        local_path = '/Users/new/science/data/otsebriy/Test_folder3'
        aws.pull_file(source_path,local_path, verbose=True)

    Inputs:
        source_path - string: full path including file name ( path as it accesses in s3)
        local_path - string: full path, as it is seen by command pwd in local directory
        local_file_name: str - provide if you want loaded file get specified name, by default it gets the source file name
        verbose [optional] - bool: send all AWS CLI outputs to screen
    Warning 1: Does not delete files, but may overwrite existing files
    Note 1 : if local folder does not exist, it will create new one
    Note 2 : local path could contain '/' at the end. It works either with or without it
    Note 3 : It is configured to allow to pull any content from science bucket

    '''

    check_aws_keys() # breaks the execution if keys pair is not provided
    source_path = validate_s3_path(source_path)
    # if not local_path:
    #     local_path =  mst.get_local_path(source_path)
    #     print ('Local path resolved to: {}'.format(local_path))

    # local_path = 'out/' # change later to something invisible
    if not local_path.endswith('/'):
        local_path += '/'

    # if local_file_name:
    #     local_path += local_file_name
    if verbose>1:
        print("Pulling s3 file: {} \n \tto destination: {}".format(source_path, local_path))

    cmd = 'aws s3 cp'.split()
    # Note sync for folders: cmd = 'aws s3 sync'.split() # changed 181021 due to not working the cp command

    cmd += [source_path, local_path]

    if verbose>1:
        bash_cmd= ' '.join(cmd)
        print('Running the command: {}\n(This may take some minutes in case of large file)'.format(bash_cmd))
    try:
        output = subprocess.check_output(cmd, universal_newlines=True) # Tell awscli to run command
        if output:
            if verbose > 1:
                print(output)
                print('Pulling file completed')

    except Exception as message:
        if verbose>1:
            print('Error occurred at calling AWS. Details: \n{}\nPulling file failed!'.format(
                message))

        else:
            print('Error occurred at calling AWS. Call function with verbose = True to get more details')


def show_bucket(target_path='s3://cond-science/',recursive=False, is_to_print = True):
    '''
    Show all objects in science bucket
    Example:
        import AWS_manager as aws
        target_path = 's3://cond-science/otsebriy/'
        aws.show_bucket(target_path, recursive=True) # running recurcive may take some minutes for high level folder

    INPUTS
      target_path - string: full path as it provided fro aws commannd
      recursive - bool: show all files in subfolders
    CAUTION: Don't forget the trailing slash!
    '''

    check_aws_keys()
    target_path = validate_s3_path(target_path)
    # Build command as list of strings
    cmd = 'aws s3 ls'.split()
    if recursive:
        cmd += ['--recursive']
    cmd += [target_path]

    bash_cmd = ' '.join(cmd)
    print('Running the command: {}'.format(bash_cmd))
    if recursive:
        print('(Note: Running recursive may take some minutes)\n')

    # Tell awscli to run command
    try:
        output = subprocess.check_output(cmd,universal_newlines=True)
        if is_to_print:
            print(output)
        return output
    except Exception as message:
        print('Error occurred at calling AWS. Details: \n{}\nPulling file failed!'.format(message))


def sn_unzip(full_path, verbose=0):
    '''
    Converts *.sz to unzipped file
    e.g.
        full_path= '/Users/new/science/data/otsebriy/serp_data/serpItemsReport.csv.sz'
        aws.sn_unzip(full_path)

    '''

    cmd = 'snzip -d {}'.format(full_path)
    if verbose:
        print('Running the command: {}'.format(cmd))

    output = subprocess.call(cmd,shell=True)
    if output == 0:
        if verbose:
            print('File decompressed')
    else:
        if verbose:
            print(output)



def get_search_analytics_report(account_id, web_property_id, period_id, target_folder_local_path=None, verbose=0):
    '''
    Checks existing file in s3, downloads from s3 if exists, and exracts the file from *.sz
    :param target_folder_local_path:  str or None that is resolved to /Users/new/science/data/otsebriy/analytics ... part pf s3 path
    :param verbose: int 0,1,or 2
    :return: True or False
    '''
    if verbose>1:
        print ('Get search_analytics report for: {}'.format(build_info_str(account_id, web_property_id, period_id = period_id)))

    bucket_name = 'cond-search-console-reports'
    s3_file_name = 'searchAnalyticsQueries.csv'
    prefix_and_key = 'prod/search_analytics_queries/v1/{}/{}/{}/{}'.format(period_id, account_id, web_property_id, s3_file_name)
    source_path = 's3://{}/{}'.format(bucket_name, prefix_and_key)

    if verbose>1:
        print ('source_path: {}'.format(source_path))

    if not target_folder_local_path:
        target_folder_local_path = os.path.join('/Users/new/science/data/otsebriy/search_analytics/{}/{}/{}'.format(
            period_id, account_id,web_property_id))
    loaded_fn_full_path = os.path.join(target_folder_local_path, s3_file_name)

    if not os.path.isfile(loaded_fn_full_path):
        if not is_file_in_s3(prefix_and_key=prefix_and_key, verbose=verbose, bucket_name=bucket_name):
            warnings.warn('File is not found: {}'.format(source_path))
            return False

        pull_file(source_path, target_folder_local_path, verbose=verbose)

        if verbose > 1:
            print('Pulling report from S3 completed. File located in {}'.format(target_folder_local_path))
    else:
        if verbose > 1:
            print('Local file found: {}'.format(loaded_fn_full_path))

    return True


def is_file_in_s3(prefix_and_key=None, prefix= None, key= None, verbose= 0, bucket_name= None): # prefix_and_key is enough
    '''
    Checks the availability of the file (object) in s3
        Note: Using client.list_objects_v2 which works far faster than s3.buckets.all()
    :param prefix_and_key: str : prefix/key: e.g. 'otsebriy/2017-09-21_dropped/Results.csv'
        Note: bucket name is not included into prefix
        Note: gets overwritten if prefix and key provided
    :param prefix: str : e.g. 'otsebriy/2017-09-21_dropped'
    :param key: str : e.g. 'Results.csv'

    e.g. 181022:
    prefix_and_key = 'prod/serp_items/v1/time_period_id=466/account_id=14/web_property_id=274/rank_source_id=1/serpItemsReport.csv.sz'
    bucket_name = 'searchlight-reports-prod'
    print (aws.is_file_in_s3(prefix_and_key=prefix_and_key ,verbose= True, bucket_name = bucket_name))
    Note: bucket_name is configured to search in 'cond-science'


    :returns: True if exists or False otherwise
    '''
    if prefix and key:
        prefix_and_key = '{}/{}'.format(prefix,key)
    if verbose>1:
        print ('\nChecking in s3: {}...'.format(prefix_and_key))
    # if not bucket_name:
    #     bucket_name = cfg.s3_bucket['bucket_name']
    # s3 = boto3.resource('s3')
    client = boto3.client('s3',
        aws_access_key_id= AWS_access_pair['AWS_ACCESS_KEY_ID'],
        aws_secret_access_key=  AWS_access_pair['AWS_SECRET_ACCESS_KEY'],
        # AWS_DEFAULT_REGION='us-east-1'
        )
    response = client.list_objects_v2(Bucket=bucket_name,Prefix=prefix_and_key)
    if verbose>1:
        print ('Response: {}'.format(response))

    for obj in response.get('Contents', []):
        if verbose>1:
            print('obj["Key"]: {}'.format(obj['Key']))
        if obj['Key'] == prefix_and_key: # prefix and key ensures the only file if exists
            if verbose>1:
                print('File is found. size= {}'.format(obj['Size']))

            return (obj['Size']>0)
    if verbose>1:
        print('File is not found in s3')
    return False

def is_folder_in_s3(prefix_and_key=None,verbose= None, bucket_name= None):
    '''
    Checks the availability of the folder (prefix) in s3
        Note: Using client.list_objects_v2 which works far faster than s3.buckets.all()
    :param prefix_and_key: str : prefix : e.g. 'otsebriy/2017-09-21_dropped/' or 'otsebriy/2017-09-21_dropped'
        Note: bucket name is not included into prefix as well as s3://
    :returns: True if exists or False otherwise
    '''
    # if prefix_and_key [-1]!= '/': # '/'
    #     prefix_and_key= prefix_and_key + '/'
    #     if verbose:
    #         print ('path corrected : {} -> {}'.format(prefix_and_key[:-1], prefix_and_key))


    if prefix_and_key [-1]== '/':
        prefix_and_key_new= prefix_and_key[:-1]
        if verbose:
            print ('path corrected : {} -> {}'.format(prefix_and_key, prefix_and_key_new))
        prefix_and_key= prefix_and_key_new

    # if not bucket_name:
    #     bucket_name = cfg.s3_bucket['bucket_name']


    # s3 = boto3.resource('s3')
    client = boto3.client('s3',
        aws_access_key_id= AWS_access_pair['AWS_ACCESS_KEY_ID'],
        aws_secret_access_key= AWS_access_pair['AWS_SECRET_ACCESS_KEY']
        # AWS_DEFAULT_REGION='us-east-1'
        )
    response = client.list_objects_v2(Bucket=bucket_name,Prefix=prefix_and_key)
    if verbose:
        print ('Response: {}'.format(response))

    if len(response.get('Contents', []))>0:
        return True

    return False
    # for obj in response.get('Contents', []):
    #     if verbose:
    #         print('obj["Key"]: {}'.format(obj['Key']))
    #     if obj['Key'] == prefix_and_key: # prefix and key ensures the only file if exists
    #         if verbose:
    #             print('File is found. Size= {}'.format(obj['Size']))
    #         return obj['Size']== 0 # all folders has size 0 in s3
    #
    # return False


def upload_file_to_s3(file_name, target_key): # no need to use
    # alternative approach using tinys3
    '''
    :param file_name:
    :param target_key: prefix  + file name e.g. Note: it does not contain bucket-name like 'cond-science'
    :return:
    '''

    bucket_name = cfg.s3_bucket['bucket_name']
    print('Uploading {} to Amazon S3 bucket {}'.format(file_name, bucket_name))
    # import tinys3
    # conn = tinys3.Connection(cfg.AWS_access_pair['AWS_ACCESS_KEY_ID'],cfg.AWS_access_pair['AWS_SECRET_ACCESS_KEY'],tls=True)
    # f = open(file_name,'rb')
    # conn.upload(target_key,f,bucket_name)

def build_serp_report_directory_path(time_period_id, account_id, web_property_id, rank_source_id):
    # works for for serp and msv reports
    return 'time_period_id={0}/account_id={1}/web_property_id={2}/rank_source_id={3}'.format(time_period_id, account_id, web_property_id, rank_source_id)

def build_analytics_report_directory_path (time_period_id, account_id, web_property_id, uuid):
    # /cond-content-insights/prod/analyticsUrlFacts/v1/WEEK
    # /481/5609/16339/618ff73b-1361-4cff-8841-16dd11104f7e/urlFactsReport.csv.gz
    return '{0}/{1}/{2}/{3}'.format(time_period_id, account_id, web_property_id, uuid)


def get_serp_report(target_report, verbose= 0):
    '''Checks existing file in s3, downloads from s3 if exists, and exracts the file from *.sz'''
    # source file e.g. s3://searchlight-reports-prod/prod/serp_items/v1/time_period_id=491/account_id=11278/web_property_id=48658/rank_source_id=5/serpItemsReport.csv.sz
    account_id, web_property_id, rank_source_id, period_id = target_report.values()

    bucket_name = 'searchlight-reports-prod'
    s3_file_name= 'serpItemsReport.csv.sz'
    prefix_and_key = 'prod/serp_items/v1/'+ build_serp_report_directory_path(period_id, account_id, web_property_id, rank_source_id) + '/'+ s3_file_name
    source_path = 's3://{}/{}'.format(bucket_name, prefix_and_key)
    # target_folder_local_path = os.path.join('/Users/new/science/data/otsebriy/serp', build_serp_report_directory_path(period_id, account_id, web_property_id, rank_source_id))  # e.g. 'time_period_id=484/account_id=7251/web_property_id=24150/rank_source_id=1'
    if not os.path.exists('../temp_files'):
        os.makedirs('../temp_files')


    target_folder_local_path = os.path.join('../temp_files/serp', build_serp_report_directory_path(period_id, account_id, web_property_id, rank_source_id))  # e.g. 'time_period_id=484/account_id=7251/web_property_id=24150/rank_source_id=1'

    # loaded_fn = prefix_and_key.split('/')[-1]
    loaded_fn_full_path = os.path.join(target_folder_local_path, s3_file_name)
    unzipped_fn_full_path = loaded_fn_full_path[:-3]

    if verbose>1:
        print ('unzipped_fn_full_path:', unzipped_fn_full_path)

    # remove later
    # os.environ['AWS_ACCESS_KEY_ID'] = "AKIAI6BXBP5FANQ247JA"
    # os.environ['AWS_SECRET_ACCESS_KEY'] = "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"

    # if not os.path.isfile(unzipped_fn_full_path):
    if not is_file_in_s3(prefix_and_key=prefix_and_key, verbose=verbose, bucket_name=bucket_name):
        warnings.warn('File is not found: {}'.format(source_path))
        return False

    pull_file(source_path, target_folder_local_path, verbose=verbose)
    sn_unzip(loaded_fn_full_path)
    if verbose>1:
        print('Pulling report from S3 completed. File located in {}'.format(target_folder_local_path))
    # else:
    #     if verbose>1:
    #         print('Local file found: {}'.format(unzipped_fn_full_path))

    return True


def get_msv_report(target_report, verbose= 0):
    '''Checks existing file in s3, downloads from s3 if exists, and exracts the file from *.sz'''
    account_id, web_property_id, rank_source_id, period_id = target_report.values()


    bucket_name = 'searchlight-reports-prod'
    s3_file_name= 'searchVolumes.csv.sz'
    prefix_and_key = 'prod/monthly_volume_trend/v2/' + build_serp_report_directory_path(period_id, account_id, web_property_id, rank_source_id) + '/'+ s3_file_name
    source_path = 's3://{}/{}'.format(bucket_name, prefix_and_key)
    target_folder_local_path = os.path.join('/Users/new/science/data/otsebriy/monthly_volume_trends', build_serp_report_directory_path(period_id, account_id, web_property_id, rank_source_id))  # e.g. 'time_period_id=484/account_id=7251/web_property_id=24150/rank_source_id=1'

    # loaded_fn = prefix_and_key.splitis_report_available = get_msv_report(target_report, verbose=2)('/')[-1]
    loaded_fn_full_path = os.path.join(target_folder_local_path, s3_file_name)
    unzipped_fn_full_path = loaded_fn_full_path[:-3]

    if not os.path.isfile(unzipped_fn_full_path):
        if not is_file_in_s3(prefix_and_key=prefix_and_key, verbose=verbose, bucket_name=bucket_name):
            warnings.warn('File is not found: {}'.format(source_path))
            return False

        pull_file(source_path, target_folder_local_path, verbose=verbose)
        sn_unzip(loaded_fn_full_path)
        if verbose>1:
            print('Pulling report from S3 completed. File located in {}'.format(target_folder_local_path))
    else:
        if verbose>1:
            print('Local file found: {}'.format(unzipped_fn_full_path))

    return True








def build_info_str(account_id, web_property_id, rank_source_id=None, period_id = None, uuid = None):
    res = 'Account_id= {}, web_property_id= {}'.format(account_id, web_property_id, rank_source_id)
    if period_id:
        res = '{}, period_id= {}'.format(res,period_id)
    if rank_source_id:
        res = '{}, rank_source_id= {}'.format(res,rank_source_id)

    if uuid:
        res = '{}, uuid = {}'.format(res, uuid)

    return res


def get_analytics_report(account_id, web_property_id, period_id, uuid, target_folder_local_path=None, verbose=0):
    '''
    Checks existing file in s3, downloads from s3 if exists, and exracts the file from *.sz
    :param target_report: tuple (account_id, web_property_id, uuid, period_id )
    :param target_folder_local_path:  str or None that is resolved to /Users/new/science/data/otsebriy/analytics ... part pf s3 path
    :param verbose: int 0,1,or 2
    :return: True or False
    '''
    if verbose>1:
        print ('Get analytics for: {}'.format(build_info_str(account_id, web_property_id, period_id = period_id, uuid= uuid)))

    # s3://cond-content-insights/prod/analyticsUrlFacts/v1/WEEK/481/5609/16339/618ff73b-1361-4cff-8841-16dd11104f7e/urlFactsReport.csv.gz
    bucket_name = 'cond-content-insights'
    s3_file_name = 'urlFactsReport.csv.gz'
    prefix_and_key = 'prod/analyticsUrlFacts/v1/WEEK/' \
                     + build_analytics_report_directory_path(period_id, account_id, web_property_id, uuid) + '/' \
                     + s3_file_name
    source_path = 's3://{}/{}'.format(bucket_name, prefix_and_key)

    if verbose>1:
        print ('source_path: {}'.format(source_path))

    # source_path = 's3://cond-content-insights/prod/analyticsUrlFacts/v1/WEEK/481/5609/16339/618ff73b-1361-4cff-8841-16dd11104f7e/urlFactsReport.csv.gz'
    if not target_folder_local_path:
        # target_folder_local_path=  os.path.join ('/Users/new/science/data/otsebriy/serp', '/'.join(source_path.split('/')[-5:-1]))  # e.g. 'time_period_id=484/account_id=7251/web_property_id=24150/rank_source_id=1'

        target_folder_local_path = os.path.join('/Users/new/science/data/otsebriy/analytics',
                                                build_analytics_report_directory_path(period_id, account_id,
                                                                                 web_property_id, uuid))
    loaded_fn_full_path = os.path.join(target_folder_local_path, s3_file_name)
    unzipped_fn_full_path = loaded_fn_full_path[:-3]

    if not os.path.isfile(unzipped_fn_full_path):
        if not is_file_in_s3(prefix_and_key=prefix_and_key, verbose=verbose, bucket_name=bucket_name):
            warnings.warn('File is not found: {}'.format(source_path))
            return False

        pull_file(source_path, target_folder_local_path, verbose=verbose)
        mst.unzip(loaded_fn_full_path)
        os.remove(loaded_fn_full_path)

    if os.path.isfile(loaded_fn_full_path): # this is temporary - ro delete the already existed zip files
        print ('Removed the remaining zip: {}'.format(loaded_fn_full_path))
        os.remove(loaded_fn_full_path)

        if verbose > 1:
            print('Pulling report from S3 completed. File located in {}'.format(target_folder_local_path))
    else:
        if verbose > 1:
            print('Local file found: {}'.format(unzipped_fn_full_path))

    return True

def get_additional_analytics_data(bucket, id_some):

    import re
    bucket_account =  os.path.join(bucket, id_some)
    results_str = show_bucket(bucket_account, recursive=True, is_to_print= False)
    results_raw = re.split(' |\n', results_str)
        # results_str.split(' ')
    results = [os.path.join('s3://cond-test/', fn) for fn in results_raw if 'analyticsWebAllDims.1.json' in fn]
    for source_path in results:
        date =source_path.split('/')[-2]
        if date.split('-')[1] in ['10', '11', '12','01']:
            target_folder_local_path = '/Users/new/science/data/otsebriy/search_analytics/{}/{}'.format(id_some,date)
            pull_file(source_path, local_path=target_folder_local_path, verbose=1)



# samples of usage
if __name__ == '__main__':
    print ('Start execution\n')
    # print('Select corresponding step(s) to run samples')

    steps= [14]

    if 1 in steps: # push_folder sample
        local_path = '/Users/new/science_1/data/otsebriy/Test_folder'
        target_path = 's3://cond-science/otsebriy/Test_bucket_folder'
        push_folder(local_path, target_path, verbose=True)

    if 2 in steps:  # push_file sample
        local_path = '/Users/new/science_1/data/otsebriy/Test_folder/all_duplicates_canonical.csv'
        target_path = 's3://cond-science/otsebriy/Test_bucket_folder/'
        push_file(local_path, target_path, verbose=True)

    if 3 in steps:  # pull_folder sample
        source_path = 's3://cond-science/otsebriy/MSV/180530' # make sure the folder exists. Otherwise nothing will get copied and no error occurs
        local_path = '/Users/new/science/data/otsebriy/Test_folder3'
        pull_folder(source_path, verbose=True)

    if 4 in steps:  # pull_file sample
        # s3://cond-msv-services/dev/msv-file-exchange-collector/2018-06-19_14-11/dff572f1-3076-4841-b4c0-23e91ec75e3a/prodex/response/results.csv.gz
        # s3://cond-msv-services/dev/msv-file-exchange-collector/2018-06-19_14-11/dff572f1-3076-4841-b4c0-23e91ec75e3a/promoua/response/results.csv.gz
        source_path = 's3://cond-msv-services/dev/msv-file-exchange-collector/2018-06-19_14-11/dff572f1-3076-4841-b4c0-23e91ec75e3a/file_exchange_results.csv'
        local_path = '/Users/new/science/data/otsebriy/MSV/2018-06-19/'
        pull_file(source_path,local_path, verbose=True)

    if 5 in steps:  # show_bucket sample
        # target_path = 's3://cond-science/otsebriy/'
        target_path = 's3://cond-test/gsc-etl-dev/v1/1462/'
        show_bucket(target_path, recursive=True) # running recurcive may take some minutes for high level folder

    if 6 in steps:
        print(validate_s3_path('hello'))
        print(validate_s3_path('s3://cond-science/avashchenko/MSV/Prodex'))
        print(validate_s3_path('s3:/cond-science/avashchenko/MSV/Prodex'))
        print(validate_s3_path('/cond-science/avashchenko/MSV/Prodex'))
        print(validate_s3_path('cond-science/avashchenko/MSV/Prodex'))
        print(validate_s3_path('avashchenko/MSV/Prodex'))
        print(validate_s3_path('/cond-science/otsebriy/MSV/2017-09-21/Prodex/sent_again_keywords_Prodex_0.csv'))
        print(validate_s3_path('/otsebriy/MSV/2017-09-21/Prodex/sent_again_keywords_Prodex_0.csv'))
        print(validate_s3_path('otsebriy/MSV/2017-09-21/Prodex/sent_again_keywords_Prodex_0.csv'))

        # source_path = 's3://cond-science/avashchenko/MSV/2017-09-21/PromoUA/set_res.csv'
        # # science.s3.amazonaws.com/avashchenko/MSV/2017-09-21/PromoUA/set_res.csv
        # date_mark =  '2017-09-21'
        # local_path = '{}{}/PromoUA/result_1.csv'.format(cfg.path['local_MSV_folder'],date_mark)
        # pull_file(source_path,local_path, verbose=True)

    if 7 in steps:  # work with s3 using boto
        print('How to use boto3: (is_file_in_s3 / is_folder_in_s3)...')
        print(is_file_in_s3(path='otsebriy/2017-09-21_dropped/Results.csv'))  # True
        print(is_file_in_s3(path='otsebriy/2017-09-21_dropped/Results.csv1'))  # False
        print('How to provide modre details:')
        print(is_folder_in_s3(path='otsebriy/2017-09-21_dropped1/', verbose=True))  # False
        print(is_folder_in_s3(path='otsebriy/2017-09-21_dropped1'))  # False

        print(is_folder_in_s3(path='otsebriy/2017-09-21_dropped/'))  # True
        print(is_folder_in_s3(path='otsebriy/2017-09-21_dropped', verbose=True))  # True

    if 8 in steps:  # alternative approach of uploading file using  tinys3 - No need to use
        file_name = '/Users/new/science/data/otsebriy/categorization_data/Demo_Sprint_4_Daniel_Craig/release/location_categories_results.zip'
        target_key = 'otsebriy/categorization_data/Demo_Sprint_4_Daniel_Craig/release/location_categories_results.zip'
        upload_file_to_s3(file_name, target_key)

    if 9 in steps: # push_file sample
        local_path = '/Users/new/science/data/otsebriy/categorization_data/Demo_Sprint_4_Daniel_Craig/release/location_categories_results.zip'
        target_path = 's3://cond-science/otsebriy/categorization_data/Demo_Sprint_4_Daniel_Craig/release/'
        push_file(local_path, target_path)

    if 10 in steps:
        # folder_name='/Users/new/science/data/otsebriy/categorization_data'
        # upload_folder(folder_name, is_to_overwrite=False, verbose=True)

        upload_folder('/Users/new/science/data/otsebriy/categorization_data/Demo_Sprint_4_Daniel_Craig/release',
                          is_to_overwrite=False, verbose=True)

    if 11 in steps:
        prefix_and_key = 'prod/serp_items/v1/time_period_id=466/account_id=14/web_property_id=274/rank_source_id=1/serpItemsReport.csv.sz'
        bucket_name = 'searchlight-reports-prod'
        print (is_file_in_s3(prefix_and_key=prefix_and_key ,verbose= True, bucket_name = bucket_name ))
        print (is_folder_in_s3(prefix_and_key=prefix_and_key,verbose= True, bucket_name = bucket_name ))

    if 12 in steps: # sample to pull the file
        source_path = 's3://searchlight-reports-prod/prod/serp_items/v1/time_period_id=466/account_id=14/web_property_id=274/rank_source_id=1/serpItemsReport.csv.sz'
        local_path = '/Users/new/science/data/otsebriy/serp_data'
        pull_file(source_path, local_path, verbose=True)

    if 13 in steps: # sample to sn_unzip
        full_path= '/Users/new/science/data/otsebriy/serp_data/serpItemsReport.csv.sz'
        sn_unzip(full_path)

    if 14 in steps: #  get_serp_report
            account_id=  11278
            web_property_id= 48658
            rank_source_id= 5
            period_id = 491
            target_report = {'account_id': account_id, 'web_property_id': web_property_id, 'rank_source_id': rank_source_id,
                         'period_id': period_id}
            is_report_available = get_serp_report(target_report, verbose=2)
        # s3://searchlight-reports-prod/prod/serp_items/v1/time_period_id=491/account_id=11278/web_property_id=48658/rank_source_id=5/serpItemsReport.csv.sz

    if 15 in steps: #  get_analytics_report
            account_id=  5609
            web_property_id= 16339
            uuid= '618ff73b-1361-4cff-8841-16dd11104f7e'
            period_id = 489
            is_report_available = get_analytics_report(account_id, web_property_id, period_id, uuid, verbose=2)


    if 16 in steps:
        account_id = 11278
        web_property_id = 48658
        rank_source_id = 5
        period_id = 488
        target_report = {'account_id': account_id, 'web_property_id': web_property_id, 'rank_source_id': rank_source_id,
                         'period_id': period_id}

    if 17 in steps:
        # s3://cond-search-console-reports/prod/search_analytics_queries/v1/463/8468/30743/searchAnalyticsQueries.csv
        account_id = 8468 # 11278
        web_property_id = 30743 # 48658
        period_id = 463 # 488
        get_search_analytics_report(account_id, web_property_id, period_id)


    if 18 in steps:
        # Getting data provided by A.Aleksieienko
        # Account_id= 5609  web_property_id= 16339 Country 'United States’
        # https://console.aws.amazon.com/s3/buckets/cond-test/gsc-etl-dev/v1/1462/?region=us-east-1&tab=overview
        bucket = '/cond-test/gsc-etl-dev/v1/'
        id_some = '1462' # '1493'

        get_additional_analytics_data(bucket, id_some)


    print('\nFinish execution')