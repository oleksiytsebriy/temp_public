
# coding: utf-8

# In[1]:


import time
from selenium import webdriver
import re
from urllib import request
import json
import pandas as pd
import numpy as np
import datetime as dt
import os
import AWS_manager as aws

import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate



heap_analytics_email= os.environ['heap_analytics_email']
heap_analytics_password= os.environ['heap_analytics_password']

gmail_user = os.environ['gmail_user'] #  'insight.stream.analytics@gmail.com'
gmail_password = os.environ['gmail_password']#  'insight.stream.analytics12345678'
send_to= os.environ['send_to']
send_to_list= send_to.split(',')


login_url = 'https://heapanalytics.com/login'


from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

connected = False

print ('Try to connect to webdriver... ')
while not connected:
    try:
        driver = webdriver.Remote("http://selenium:4444/wd/hub", DesiredCapabilities.CHROME)
        connected = True
    except:
        print('Selenium is not started yet, trying again in 2s...')
        time.sleep(2)

    try: # provided possibility to run on local machine
        driver = webdriver.Chrome('/Users/new/temp/chromedriver')
        connected = True
    except:
        pass

print ('Connected to webdriver.')
# driver = webdriver.Remote("http://127.0.0.1:4444/wd/hub", DesiredCapabilities.CHROME)

print ('Requesting login page...')
driver.get(login_url)
driver.find_element_by_id('email').send_keys(heap_analytics_email)
driver.find_element_by_id ('password').send_keys(heap_analytics_password)
print ('Logging in...')

login_button = driver.find_elements_by_xpath('//*[@id="login"]/div/div/fieldset/div[3]/button')
login_button[0].submit()
time.sleep (10)

print ('Requesting target report...')
target_url = 'https://heapanalytics.com/app/graph/report/IS-Analytics-Automated-902273'
driver.get(target_url)

time.sleep (10)
print ('Extracting report head...')
# extract table columns   
head = driver.find_elements_by_xpath("//*[@id='workspace']//*[@class='report']//*[@class='tableContainer']/table/thead/tr/th")
table_head = [res.text for res in head]

print ('Extracting report values...')
# extract values  
values = driver.find_elements_by_xpath("//*[@id='workspace']//*[@class='report']//*[@class='tableContainer']/table/tbody/tr")
report = [[td.text for td in  tr.find_elements_by_tag_name("td")]  for tr in values]
driver.quit()

table_head = [re.findall(r'[\w -]+', col)[0] for col in table_head]

df_heap_analytics = pd.DataFrame(report, columns = table_head)
print ('Top records of scraped report:\n{}'.format(df_heap_analytics.head(10)))

def stamp2date(time_stamp):
    if type(time_stamp)== str:
        time_stamp= int (time_stamp)
    if len(str(time_stamp))== 13:
        time_stamp= time_stamp/1000
    return dt.datetime.fromtimestamp(time_stamp).strftime("%d/%m/%Y") 
    
# stamp2date(19150067800)

def date2stamp(date_str):
    return int(dt.datetime.strptime(date_str, "%d/%m/%Y").timestamp())

# date_str= '22/07/2017'
# print (date_str)
# print (date2stamp(date_str))
# print ('check:', stamp2date(date2stamp(date_str)))
# assert (stamp2date(date2stamp(date_str)) == date_str)

def compute_last_date_by_day_name(day_name):  
    today = dt.datetime.today()
    days_names = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su']
    day_number = days_names.index(day_name)
    offset = (today.weekday() - day_number) % 7
    target_last_day = today - dt.timedelta(days=offset)
    print ('Last Sunday date: {}, month = {}, day= {} year = {}'.format(target_last_day.strftime("%d/%m/%Y"), target_last_day.month, target_last_day.day,  target_last_day.year))
    return  target_last_day.strftime("%d/%m/%Y")

# end_period = compute_last_date_by_day_name(day_name= 'su')
# print (end_period)


# <font color = green >
# 
# ## Build start and end period time stamps
# 
# </font>
# 

# In[4]:


def create_time_stamps(start_date=None,end_date= None):
    '''
    start_date: None or string in format "%d/%m/%Y" e.g. 03/02/2019 
    if None returns last su 
    end_date:  None or string in format "%d/%m/%Y" e.g. 10/02/2019
    if None returns 7 days before end_date  
    '''
    # compute period end           
    if end_date: 
        end_period = end_date
        end_period_date = dt.datetime.strptime(end_period, "%d/%m/%Y")

    else: 
        last_su = compute_last_date_by_day_name(day_name= 'su')        
        # str to date 
        last_su_date = dt.datetime.strptime(last_su, "%d/%m/%Y")
        
        #  need to check the report from heap corresponds to data from solr          
        last_sa_date= last_su_date - dt.timedelta(days=1)

        
        # need period of publish - week before of events observed 
        end_period_date = last_su_date - dt.timedelta(days=7)
        end_period = end_period_date.strftime("%d/%m/%Y") 
        print ("end_period_date = last_su - 7 days : ", end_period_date) 

    end_period_stamp = date2stamp(end_period)
    end_period_stamp_solr = '{}000'.format(end_period_stamp)
    print ('end stamp: {} -> {} -> {}'.format(end_period, end_period_stamp, end_period_stamp_solr))

    # compute period start       
    if start_date:
        start_period =  start_date
    else:
        # delta days 
        start_period_date = end_period_date - dt.timedelta(days=7)
        print ("start_period_date = end_period_date - 7 days : ", start_period_date) 

        # date to str 
        start_period = start_period_date.strftime("%d/%m/%Y")

    start_period_stamp =  date2stamp(start_period)
    start_period_stamp_solr = '{:.0f}000'.format(start_period_stamp)
    print ('start_period : {} -> {} -> {}'.format(start_period, start_period_stamp, start_period_stamp_solr))

    print ('publishTimestamp:[{} TO {}]'.format(start_period_stamp_solr, end_period_stamp_solr))
    print ('check the dates values from time stamps: \n{}\n{}'.format(
    stamp2date(start_period_stamp_solr), stamp2date(end_period_stamp_solr)))
    return start_period_stamp_solr, end_period_stamp_solr, last_sa_date
    
    
create_time_stamps(start_date=None,end_date= None)
# create_time_stamps(start_date='16/05/2016',end_date='10/02/2019')


# <font color = green >
# 
# ## Check the resolved period matches Heap Analytics period 
# 
# </font>
# 
# 

# In[14]:


considered_period_heap = table_head[-3]
start_period_stamp_solr, end_period_stamp_solr, last_sa_date = create_time_stamps()
def check_matched_period(last_sa_date, considered_period_heap):
    mon = last_sa_date.strftime("%b")
    day = str(int(last_sa_date.strftime("%d"))) # avoid zero headed 
    return mon in considered_period_heap and  considered_period_heap.endswith(day)
       
print ('\nSolr (publishing) period should be week before.')
print ('Heap Analyitics (events) period: {}'.format(considered_period_heap))

if not check_matched_period(last_sa_date, considered_period_heap):
    considered_period_heap= table_head[-2] # assume this is the case when heap analytics doeas not haver yet new period events 



# <font color = green >
# 
# ## Query the story_cards statistics 
# 
# </font>

# In[6]:


# start_period_stamp_solr, end_period_stamp_solr = create_time_stamps()

print ('Requesting data that is week before {}'.format(considered_period_heap))

# Assume the pubkishing is perfromed on Sa - this it requests from Solr the publishing for week before Heap Analytics 


query_url= 'http://prod-deck-solr-0.battery-park.conductor.com:8983/solr/story_cards/select?indent=on&q=publishTimestamp:[{}%20TO%20{}]&rows=0&facet=on&facet.field=storyCardType&wt=json'.format(
start_period_stamp_solr, end_period_stamp_solr)

connection = request.urlopen(query_url)
response= connection.read()
results  = json.loads(response)
raw_results = results['facet_counts']['facet_fields']['storyCardType']
total = results['response']['numFound']
names = raw_results[::2]
values = raw_results[1::2]
solr_df = pd.DataFrame({'CardType':names, 'Published': values})
solr_df =  solr_df[solr_df['Published']>0]


# In[22]:


df_one_period= df_heap_analytics[['CardType', 'EventName', considered_period_heap]]  # Note: consider completed period  [-1] total , [-2] current incomplete period, [-3] - last completed period
print ('\nConsidering columns: {}'.format(list (df_one_period)))
print ('top records: \n{}'.format(df_one_period.head()))

df_one_period= df_one_period.copy()
df_one_period.columns = [list (df_one_period)[0], list (df_one_period)[1], 'value'] # [CardType, EventName, value]

# # df['EventName']= df['EventName'].str.replace('None', 'Other')
df_one_period['EventName']= df_one_period['EventName'].apply(lambda x: 'None' if len(x)==0 else x.replace('Insight Stream ',''))

# explicitly remove None  including empty
df_one_period= df_one_period[df_one_period['EventName']!='None']


# make pivot table

table = pd.pivot_table(df_one_period, index=['CardType'], columns =['EventName'], values=['value'], aggfunc=sum, fill_value=0)

# make the columns 1 level 
table.columns = [col[1] for col in list (table)] 
# convert to numbers 
table = table.apply(lambda x: x.str.replace(',', '').astype(float), axis=0) 
table = table.fillna(0)
table = table.astype(int)
table.head(20)


def send_mail(send_from, send_to, subject, text, files=None):
    assert isinstance(send_to, list)

    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(text))

    for file in files:  # or []:
        with open(file, "rb") as f:
            part = MIMEApplication(
                f.read(),
                Name=basename(file)
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="{}"'.format(basename(file))
        msg.attach(part)

    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)

    server.sendmail(send_from, send_to, msg.as_string())
    server.close()
    print('Email sent to {}'.format(send_to_list))


def compute_metrics(df):
    cols_order = ['CardType']
    CTA_cols = []
    if 'Click on CTA 0' in list(df) and 'Click on CTA 1' in list(df):
        df['Combined Clicks'] = df['Click on CTA 0'] + df['Click on CTA 1']
        CTA_cols += ['Click on CTA 0', 'Click on CTA 1', 'Combined Clicks']
    elif 'Click on CTA 0' not in list(df):
        df['Combined Clicks'] = df['Click on CTA 1']
        CTA_cols += ['Click on CTA 1', 'Combined Clicks']
    elif 'Click on CTA 1' not in list(df):
        df['Combined Clicks'] = df['Click on CTA 0']
        CTA_cols += ['Click on CTA 0', 'Combined Clicks']

    base_cols = ['CardType', 'Published', 'Card View', 'Click on CTA 0',
                 'Click on CTA 1']  # list of cols not to apply % computation
    event_cols = [col for col in list(df) if col not in base_cols]

    if 'Card View' in list(df):
        df['View/Published %'] = (df['Card View'] / df['Published']).apply(
            lambda x: '-' if np.isnan(x) or x == np.inf else '{:.2%}'.format(x))
        cols_order = cols_order + ['Card View', 'Published', 'View/Published %'] + CTA_cols
        df['Combined Clicks %'] = (df['Combined Clicks'] / df['Card View']).apply(
            lambda x: '-' if np.isnan(x) or x == np.inf else '{:.2%}'.format(x))
        cols_order += ['Combined Clicks %']

        for col in event_cols:
            if col == 'Combined Clicks': continue
            df['{} %'.format(col)] = (df[col] / df['Card View']).apply(
                lambda x: '-' if np.isnan(x) or x == np.inf else '{:.2%}'.format(x))
            cols_order += [col, '{} %'.format(col)]

    return df[cols_order]


df_merged = pd.merge(table, solr_df, left_index=True, right_on='CardType', how='right')
df_merged = df_merged.fillna(0)
df_merged = df_merged.sort_values('CardType')
total = df_merged.sum(axis=0)
total['CardType'] = 'Total'

df_merged = df_merged.append(total, ignore_index=True)

result_df = compute_metrics(df_merged)

print ('\nResult report:\n ', result_df.head())

if not os.path.exists('../temp_files'): #  this allows to avoid those files get to docker after running locally
    os.makedirs('../temp_files')

result_file_name = '../temp_files/IS_report_{}_{}.xlsx'.format(considered_period_heap.replace(' ',''), dt.datetime.today().strftime('%y%m%dT%H%M'))
print ('Saving report to {}.'.format(result_file_name))
writer = pd.ExcelWriter(result_file_name, engine='xlsxwriter')

# Convert the dataframe to an XlsxWriter Excel object.
result_df.to_excel(writer, sheet_name='Sheet1', index=False)

# Get the xlsxwriter workbook and worksheet objects.
workbook = writer.book
worksheet = writer.sheets['Sheet1']

# Add cell formats.
format_BC = workbook.add_format(
    {'num_format': '#,##0.00', 'align': 'right', 'fg_color': '#d5f4e6'})  # 'fill color': '#87CEFA'})
format_D = workbook.add_format({'align': 'right', 'fg_color': '#d5f4e6'})
format_EH = workbook.add_format({'align': 'right', 'fg_color': '#80ced6'})
format_IZ = workbook.add_format({'align': 'right'})

format_total_A = workbook.add_format({'bold': True, 'border': 1})
format_total_BC = workbook.add_format(
    {'num_format': '#,##0.00', 'align': 'right', 'fg_color': '#d5f4e6', 'bold': True, 'border': 1})
format_total_D = workbook.add_format({'align': 'right', 'fg_color': '#d5f4e6', 'bold': True, 'border': 1})
format_total_EH = workbook.add_format({'align': 'right', 'fg_color': '#80ced6', 'bold': True, 'border': 1})
format_total_IZ = workbook.add_format({'align': 'right', 'bold': True, 'border': 1})

# Note: It isn't possible to format any cells that already have a format such
# as the index or headers or any cells that contain dates or datetimes.

# Set the column width and format.
worksheet.set_column('B:C', 10, None)
worksheet.set_column('D:D', 10, None)
worksheet.set_column('A:A', 24, None)
worksheet.set_column('E:H', 10, None)
worksheet.set_column('I:Z', 10, None)

header_format = workbook.add_format({
    'bold': True,
    'text_wrap': True,
    'valign': 'top',
    'fg_color': '#fefbd8',
    'border': 1})

comment_format = workbook.add_format({
    'bold': True,
    'font': {'color': 'red',
             'size': 14},
    'fg_color': '#fefbd8',
    'border': 1})

# Write the column headers with the defined format.
for col_num, value in enumerate(result_df.columns.values):
    worksheet.write(0, col_num, value, header_format)
    if '%' in value:
        if '/Published' in value:
            worksheet.write_comment(0, col_num, value, None )
        else:
            worksheet.write_comment(0, col_num, value.replace(' %', ' / Card View %' ), None )

for i in range(len(result_df)):
    for col_num, value in enumerate(result_df.iloc[i]):
        if col_num == 0:
            worksheet.write(i + 1, col_num, value, None)
        if col_num == 1 or col_num == 2:
            worksheet.write(i + 1, col_num, value, format_BC)
        if col_num == 3:
            worksheet.write(i + 1, col_num, value, format_D)
        if col_num > 3 and col_num < 8:
            worksheet.write(i + 1, col_num, value, format_EH)
        if col_num >= 8:
            worksheet.write(i + 1, col_num, value, format_IZ)

for col_num, value in enumerate(result_df.iloc[-1]):
    if col_num == 0:
        worksheet.write(len(result_df), col_num, value, format_total_A)
    if col_num == 1 or col_num == 2:
        worksheet.write(len(result_df), col_num, value, format_total_BC)
    if col_num == 3:
        worksheet.write(len(result_df), col_num, value, format_total_D)
    if col_num > 3 and col_num < 8:
        worksheet.write(len(result_df), col_num, value, format_total_EH)
    if col_num >= 8:
        worksheet.write(len(result_df), col_num, value, format_total_IZ)

# worksheet.write(len(result_df)+2,0, 'Note: All percentage columns (except View/Published %) are based on the card view field e.g. Combined Clicks % = Combined Clicks / Card View', header_format)

# Close the Pandas Excel writer and output the Excel file.
writer.save()

target_path = 's3://cond-science/otsebriy/insight_stream/analytics/'
aws.push_file(result_file_name, target_path)


text= ''
subject = 'Insight stream analytics report for {}'.format(considered_period_heap)
send_from  = gmail_user
# send_to  = ['otsebriy@conductor.com', 'ol.tsebriy@gmail.com', 'zebra.lm@gmail.com']

files = [result_file_name]
send_mail(send_from, send_to_list, subject, text, files=files)

